[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg)](http://commitizen.github.io/cz-cli/)

# Alltruck API #

This are the steps to get the server up and running

##  Libraries we are using: ##

### Production: ###
* [Express](http://expressjs.com/es/)
* [Async](https://github.com/caolan/async)
* [Babel](https://babeljs.io/)
* [Bcrypt](https://www.npmjs.com/package/bcrypt)
* [Body-parser](https://github.com/expressjs/body-parser)
* [Chalk](https://github.com/chalk/chalk)
* [JWT](https://github.com/auth0/node-jsonwebtoken)
* [Lodash](https://lodash.com/)
* [Mongoose](http://mongoosejs.com/docs/guide.html)
* [Stripe](https://stripe.com/docs/api/node)

### Development: ###
* [Eslint](http://eslint.org/)
* [Babel-eslint](https://github.com/babel/babel-eslint)
* [Nodemon](http://nodemon.io/)


### How do I get set up? ###

* Download the repo
* *cd* to the project 
* yarn install (install dependecies)
* yarn run dev (run the server)
* happy coding! :)

before do a pull request please run:

```
yarn run pretest
```

and fix the style errors or warnings

### Who do I talk to? ###

* andres.garcia@dnamicworld.com
* jorge.paiz@dnamicworld.com 

