// Third party modules
import _ from 'lodash';
const fs = require('fs');
import Bitly from 'services/bitly';

import disposableDomains from 'disposable-email-domains';
import { Converter } from 'csvtojson';
import { errorDeleting } from './defaultMessages';
import { badRequest } from './errors';

const Services = {
  isDisposableEmail : (email) => !!disposableDomains.find((domain) => email.includes(domain)),
  decimalFlagsToResponseCode (base, flags) {
    const binary = flags.join('');
    return base + parseInt(binary, 2);
  },
  flagActive : (flags) => flags.some((flag) => flag === 1 ),
  diffCond   : (val) => _.isObject(val) && !Services.plainArray(val),
  difference : (object, base) => _.transform(object, (result, value, key) => {
    if (!_.isEqual(value, base[key])) {
      result[key] = (Services.diffCond(value) && Services.diffCond(base[key])) ? Services.difference(value, base[key]) : value;
    }
  }),
  plainArray : (obj) => Array.isArray(obj) && obj.every((el) => !Array.isArray(el) && typeof el !== 'object' && typeof el !== 'function'),
  csvReload (Model, csvPath) {
    const converter = new Converter({});
    const collectionName = Model.collection.collectionName;
    return new Promise((resolve, reject) => {
      converter.on('end_parsed', (jsonResults) => {
        Model.insertMany(jsonResults)
          .then((categories) => resolve(categories))
          .catch((err) => reject(badRequest(`Error creating ${collectionName}`, err)));
      });
      Model.remove({}, (err) => {
        if (err) reject(errorDeleting(collectionName));
        const rs = fs.createReadStream(csvPath);
        rs.on('error', readError => reject(readError));
        rs.pipe(converter);
      });
    });
  },
  shortUrl : (longUrl) => new Promise((resolve, reject) =>
    Bitly.shorten({ longUrl }, (err, results) => {
      if (err) reject(err);
      const response = JSON.parse(results);
      resolve(response.data.url);

    })),
  parseError : (err) => ({ name : err.name, message : err.message }),
};

export default Services;
