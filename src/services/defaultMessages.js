import { elementSingularity } from 'services/helpers';

const Messages = {
  found : (collection = '', data ) => {
    const elementsUnits = elementSingularity(data);
    return `${collection} ${elementsUnits} found`;
  },
  schemaValidation : 'validation error',
  passUpdated      : (collection) => `${collection} password successfully changed`,
  created          : (collection) => `New ${collection} element created`,
  deleted          : (collection) => `${collection} element successfully deleted`,
  updated          : (collection) => `${collection} element successfully updated`,
  errorDeleting    : (collection) => `Error deleting ${collection} element(s)`,
  createErrorMsg   : (collection) => `Failed to create  ${collection} element(s)`,
  updateErrorMsg   : (collection) => `Failed to update  ${collection} element(s)`,
};

export default Messages;
