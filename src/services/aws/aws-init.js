import globalAWS from 'aws-sdk/global';
import SES from 'aws-sdk/clients/ses';
import SNS from 'aws-sdk/clients/sns';
import Constants from 'services/constants';
const { AWS_REGION, AWS_SECRET, AWS_KEY } = Constants;


globalAWS.config.apiVersions = {
  sns : '2010-03-31',
  // other service API versions
};

globalAWS.config.update({
  region          : AWS_REGION,
  accessKeyId     : AWS_KEY,
  secretAccessKey : AWS_SECRET,
});


const awsServices = {
  SES : new SES({ apiVersion : '2010-12-01' }),
  SNS : new SNS(),
};

const smsAttributes = {
  attributes : {
    DefaultSMSType : 'Transactional',
  },
};

awsServices.SNS.setSMSAttributes(smsAttributes, (err) => {
  if ( err) console.log(err, err.stack); // an error occurred
});

export default awsServices;
