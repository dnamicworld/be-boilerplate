// In house modules
import { SNS } from './aws-init';

const awsNotification = {

  sendSms : (PhoneNumber, Message, Subject) => new Promise( (resolve, reject) => {

    const params = {
      PhoneNumber,
      Message,
      Subject,
      MessageStructure : 'string',
    };

    SNS.publish(params, (err, data) => {
      if (err) reject(err);
      resolve(data);
    });

  }),
};

export default awsNotification;
