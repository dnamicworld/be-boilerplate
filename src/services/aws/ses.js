// In house modules
import { SES } from './aws-init';
import Constants from 'services/constants';

const destinationParam = (destination) => typeof destination === 'string' ? { ToAddresses : [destination] } : destination;
const getTemplateMailParams = (Source, destination, Template, TemplateData) => {

  const Destination = destinationParam(destination);

  return { Source, Destination, Template, TemplateData };

};

const getMailParams = (Source, destination, subject, Data) => {

  const Destination = destinationParam(destination);

  return { Source, Destination, Message : { Body : { Text : { Data } }, Subject : { Data : subject } } };

};

const templateParams = ({ templateName, htmlPart, subjectPart }) => ({
  Template : {
    TemplateName : templateName,
    HtmlPart     : htmlPart,
    SubjectPart  : subjectPart,
  },
});

const awsMailer = {
  sendTemplateMail : (destination, template, variables, from = Constants.AWS.EMAIL_SENDER) => {
    const params = getTemplateMailParams(from, destination, template, JSON.stringify(variables));
    const sendPromise = SES.sendTemplatedEmail(params).promise();
    sendPromise.then(
      (data) => {
        console.log('sendTemplateMail success: ', data);
      }).catch((err) => {
      console.log('sendTemplateMail error: ', err);
    });
  },
  sendEmail : (destination, subject, message, from = Constants.AWS.EMAIL_SENDER) => {
    const params = getMailParams(from, destination, subject, message);
    const sendPromise = SES.sendEmail(params).promise();
    sendPromise.then(
      (data) => {
        console.log('sendRawMail success: ', data);
      }).catch((err) => {
      console.log('sendRawMail error: ', err);
    });
  },
  destinationWithCC : (to, cc = Constants.AWS.CC) => ({ CcAddresses : [cc], ToAddresses : [to] }),
  createTemplate    : (data) => SES.createTemplate(templateParams(data)).promise(),
  getTemplate       : (data) => SES.getTemplate(data).promise(),
  listTemplates     : (data) => SES.listTemplates(data).promise(),
  updateTemplate    : (data) => SES.updateTemplate(templateParams(data)).promise(),
  deleteTemplate    : (data) => SES.deleteTemplate(data).promise(),
};

export default awsMailer;
