// AWS modules
import { sendEmail, sendTemplateMail } from 'services/aws/ses';
import Constants from 'services/constants';
import { getLang } from 'services/helpers';

const {
  AWS_TEMPLATES : {
    CONFIRM_EMAIL,
    DOCS_VERIFICATION,
    FORGOT_PASSWORD,
    JOURNEY,
    PAYMENT_ERROR,
    RESEND_VERIFICATION,
    WELCOME,
  },
  TRANSLATIONS : { EMAILS : { SUBJECTS, ERROR_EMAIL } },
} = Constants;

const sendEmailByTemplate = (template) => (destination, variables) => sendTemplateMail(destination, template, variables);
const sendRawEmail = (destination, subject, message) => sendEmail(destination, subject, message);

const multilanguageParams = (multiLangSub, variables) => {
  const { language, ...emailVariables } = variables;
  return {
    subject      : getLang(multiLangSub, language),
    languageCode : { [language] : true },
    ...emailVariables,
  };
};

export default ({
  confirmEmail : (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.CONFIRM_EMAIL, variables);
    return sendEmailByTemplate(CONFIRM_EMAIL)(destination, wholeVariables);
  },
  docsVerifiedEmail : (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.DOCS_VERIFICATION, variables);
    return sendEmailByTemplate(DOCS_VERIFICATION)(destination, wholeVariables);
  },
  forgotPassEmail : (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.FORGOT_PASSWORD, variables);
    return sendEmailByTemplate(FORGOT_PASSWORD)(destination, wholeVariables);
  },
  paymentErrorEmail       : (destination, variables) => sendEmailByTemplate(PAYMENT_ERROR)(destination, variables),
  receiptEmail            : (destination, variables) => sendEmailByTemplate(JOURNEY)(destination, variables),
  resendVerificationEmail : (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.RESEND_VERIFICATION, variables);
    return sendEmailByTemplate(RESEND_VERIFICATION)(destination, wholeVariables);
  },
  welcomeEmail : (destination, variables) => {
    const wholeVariables = multilanguageParams(SUBJECTS.WELCOME, variables);
    return sendEmailByTemplate(WELCOME)(destination, wholeVariables);
  },
  rawErrorEmail : (lang) => (destination, rideId) => {
    const emailParams = ERROR_EMAIL(lang); // eslint-disable-line new-cap
    return sendRawEmail(destination, `${emailParams.SUBJECT} #${rideId}`, emailParams.COPY);
  },
  rawEmail : (destination, subject, message) => sendRawEmail(subject, destination, message),
});
