import { sendSms } from 'services/aws/sns';
import pushNotifications from './pushNotifications';
import emails from './emails';

import { issueToken } from 'api/auth/services';
import { generatePhoneCode } from 'services';
import Constants from 'services/constants';
import { getLang } from 'services/helpers';

const {
  TRANSLATIONS : { PAYMENT_ERROR, SMS : { VERIFICATION } },
  TOKENS_TIME: { ACTIVATE },
} = Constants;


const notificationServices = {
  sms : {
    confirmSms          : (phone, urlShorten) => sendSms(phone, urlShorten, 'Confirmation SMS'),
    sendVerificationSms : (tokenInfo = {}, phone, language) => new Promise( async (resolve, reject) => {
      const phoneCode = await generatePhoneCode();
      const verificationMsg = `${getLang(VERIFICATION, language)} ${phoneCode}`;
      sendSms(phone, verificationMsg, 'Verification SMS')
        .then(() => resolve(issueToken({ ...tokenInfo, phone, phoneCode }, ACTIVATE)), (error) => reject(error));
    }),
    errorSms : (phone, lang) => sendSms(phone, PAYMENT_ERROR[lang], 'Error SMS'),
  },
  emails,
  pushNotifications,
};

export default notificationServices;
