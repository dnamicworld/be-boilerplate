import mongoose from 'mongoose';
import _ from 'lodash';

const validators = {
  password           : (password) => /^([a-zA-Z0-9]{8,})$/.test(password),
  name               : (name) => /^(?:[a-zA-Z0-9]\s?[a-zA-Z0-9]?){2,}$/.test(name),
  countryCode        : (code) => /^[A-Z]{3}$/.test(code), // country code iso https://en.wikipedia.org/wiki/ISO_3166-1_alpha-3
  createUpdateUnique : function createUpdateUnique (SchemaName, condition, value, done) {
    const [propName] = Object.keys(condition);
    let currCondition = condition;
    const isQuery = this instanceof mongoose.Query;
    let doc = this;
    if (isQuery) { // update query
      doc = this.getUpdate();
      currCondition = { $and : [currCondition, { _id : { $ne : doc._id } }] };
    } else { // document validation(create or save)
      if (!doc.isModified(propName)) return done(true);
    }
    return validators.uniqueness(SchemaName, currCondition, done);
  },
  uniqueAtribute : function uniqueAtribute (SchemaName, condition, value, done) {
    const [propName] = Object.keys(condition);
    let currCondition = condition;
    const isQuery = this instanceof mongoose.Query;
    let doc = this;
    const meetCondition = _.isEqual(value, Object.values(condition)[0]);
    if (isQuery) { // update query
      doc = this.getUpdate();
      if (meetCondition) { // value in true
        currCondition = { $and : [currCondition, { _id : { $ne : doc._id } }] };
      }
    } else { // document validation(create or save)
      if (!doc.isModified(propName)) return done(true);
    }
    if (!meetCondition) return done(true);
    return validators.uniqueness(SchemaName, currCondition, done);
  },
  uniqueness : (schemaName, property, cb) => {
    mongoose.model(schemaName).countDocuments(property, (err, count) => {
      if (err) return cb(err);
      // If `count` is greater than zero, "invalidate"
      return cb(!count);
    });
  },
};

export default validators;
