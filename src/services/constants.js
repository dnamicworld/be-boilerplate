const {
  SNS_KEY_ID, SNS_ACCESS_KEY,
  GREEN_USER, GREEN_PASS,
  AWS_REGION, AWS_SECRET, AWS_KEY,
  BUCKET, S3_SECRET, S3_ID, S3_URL } = process.env;

import TRANSLATIONS from './translations';
import { setDefaultLang } from 'services/helpers';

const Constants = {
  // STANDARD CODES
  // 2XX Success
  STATUS_OK              : 200,
  STATUS_CREATED         : 201,
  STATUS_ACCEPTED        : 202,
  STATUS_DELETED         : 204,
  STATUS_IM_USED         : 226, // Used as last reference to provide custom success codes
  // 3XX Redirection
  STATUS_FOUND           : 302,
  // 4XX Client errors
  STATUS_BAD_REQUEST     : 400,
  STATUS_UNAUTHORIZED    : 401,
  STATUS_FORBIDDEN       : 403,
  STATUS_NOT_FOUND       : 404,
  STATUS_INVALID_TOKEN   : 498,
  STATUS_TOKEN_REQUIRED  : 499,
  // 5XX Server errors
  STATUS_INTERNAL_ERROR  : 500,
  STATUS_NOT_IMPLEMENTED : 501,
  STATUS_BAD_GATEWAY     : 502,


  // App error codes
  ERROR_PASSWORD_LENGTH         : 411, // unused
  ERROR_EMAIL_BLACKLIST         : 423, // unused
  ERROR_EMAIL_USED_IN_FB        : 460, // unused
  ERROR_EMAIL_USED              : 461,
  ERROR_EMAIL_NOT_VERIFIED      : 462,
  ERROR_EMAIL_REQUIRED          : 463,
  ERROR_PASSWORD_REQUIRED       : 464,
  ERROR_PASSWORD_EMAIL_REQUIRED : 465,
  ERROR_OTHERS_UNFORMATED       : 466, // unused
  ERROR_FBID_REQUIRED           : 467,
  ERROR_FBTOKEN_REQUIRED        : 468,
  ERROR_EMAIL_DOES_NOT_EXIST    : 470,
  ERROR_INVALID_CREDENTIALS     : 471,
  ERROR_INVALID_TOKEN           : 472,
  ERROR_EMAIL_VERIFIED          : 473,
  ERROR_PHONE_REQUIRED          : 474,
  ERROR_EMAIL_PHONE_NOT_EXIST   : 475,
  ERROR_PHONE_NOT_VERIFIED      : 476,
  ERROR_PHONE_VERIFIED          : 477,
  ERROR_DOCS_NOT_VERIFIED       : 480,
  ERROR_DEACTIVATED             : 481,
  ERROR_PHONE_USED              : 483,
  ERROR_PHONE_AND_EMAIL_USED    : 484,
  ERROR_FBDATA_NEEDED_UNSHARED  : 485,
  ERROR_ADDING_CREDITCARD       : 489,
  ERROR_REMOVING_CREDITCARD     : 490,

  // ERROR CODE CONSTANTS
  ERROR_CODE_UNKNOWN          : 600,
  ERROR_CODE_VALIDATION_ERROR : 601,

  // USER ERROR CODE CONSTANS
  ERROR_USER_EMAIL_ALREADY_EXISTS   : 700,
  ERROR_USER_NOT_ACCOUNT_ASSOCIATED : 702,
  ERROR_USER_NOT_VERIFIED           : 703,

  // API SUCCESS messages


  LOG_ACTION_TYPES : {
    CREATE : 'CREATE',
    UPDATE : 'UPDATE',
    DELETE : 'DELETE',
    ERROR  : 'ERROR',
  },

  // SNS CONSTANTS
  SNS_KEY_ID,
  SNS_ACCESS_KEY,

  // S3 CONSTANTS
  BUCKET,
  S3_SECRET,
  S3_ID,
  S3_URL,

  // SES CONSTANTS
  AWS_REGION,
  AWS_SECRET,
  AWS_KEY,
  // GREEN PAYMENT CONSTANTS
  GREENPAYMENT : {
    URL  : 'https://sandbox.greenpay.me/',
    USER : GREEN_USER,
    PASS : GREEN_PASS,
  },
  AWS : {
    EMAIL_SENDER : 'Alltruck <info@alltruck.club>',
    CC           : 'Alltruck invoice <invoice@alltruck.club>',
    ERROR_MAIL   : 'Alltruck errors <errors@alltruck.club>',
  },

  AWS_TEMPLATES : {
    CONFIRM_EMAIL       : 'confirmEmail_subject', // ya
    DOCS_VERIFICATION   : 'docsConfirmation_subject',
    FORGOT_PASSWORD     : 'forgotPassword_subject', // ya
    JOURNEY             : 'journey_subject',
    PAYMENT_ERROR       : 'paymentError', // no se ocupa
    RESEND_VERIFICATION : 'resendVerification_subject',
    RECEIPT             : 'receipt', // no se ocupa
    WELCOME             : 'welcome_subject', // ya
  },

  // DISTANCE CONSTANTS
  DISTANCE : {
    METERS     : 'm',
    KILOMETERS : 'km',
    MILES      : 'mi',
    DEFAULT    : 'mi',
  },

  // ONESIGNAL CONSTANTS
  OS : {
    APP_ID   : '42e8f92f-d463-4bfc-8b78-4ba172506920',
    API_KEY  : 'NDRkZDkwMjQtMTM4ZC00ZjUzLTgyNTctNDYxOWE2YjYzOWE0',
    SEGMENTS : {
      ALL            : 'All',
      ACTIVE_USERS   : 'Active Users',
      INACTIVE_USERS : 'Inactive Users',
    },
    PATH   : '/api/v1/notifications',
    METHOD : 'POST',
  },

  ROUTES : {
    USER : {
      'GET' : [
        '/status',
        '/users/check',
        '/users/password',
        '/users/resend',
        '/users/sendVerification',
        '/users/verify',
        '/logs/getByDate',
      ],
      'POST' : [
        '/users',
        '/drivers',
        '/auth/user',
        '/users/fb',
        '/users/validatePhone',
        '/user/resetpassword',
      ],
    },
  },

  TRANSACTION_STATUS : {
    success : 'success',
    error   : 'failed',
  },
  CMS_URL     : (env) => (env === 'development' || env === 'dev-stable') ? Constants.CMS_DOMAINS.DEV : Constants.CMS_DOMAINS.PROD,
  CMS_DOMAINS : {
    LOCAL   : 'http://localhost:3000',
    DEV     : 'https://cms-dev.alltruckcr.com',
    STAGING : 'https://cms-staging.alltruckcr.com',
    PROD    : 'https://cms.alltruckcr.com',
  },
  TOKENS_TIME : {
    AUTH     : '24h',
    REFRESH  : '2016h', // ((24*7)* 4)* 3 = 3 Months
    FORGOT   : '24h',
    ACTIVATE : '24h',
  },
  CRON_RULES : { // FYI min 1 minute in cron jobs
    EVERY_1_MINUTE   : '*/1 * * * *',
    EVERY_5_MINUTE   : '*/5 * * * *',
    EVERY_10_MINUTE  : '*/10 * * * *',
    EVERY_25_MINUTES : '*/25 * * * *',
    EVERY_50_MINUTES : '*/50 * * * *',
    ONCE_DAY         : '* * 1/1 * *',
  },
  FALLBACK_LANG : 'ES',
  TRANSLATIONS,
};

const { FALLBACK_LANG } = Constants;
const { EMAILS : { SUBJECTS }, SMS, RIDE_NOTIFICATION } = TRANSLATIONS;

Constants.TRANSLATIONS.EMAILS.SUBJECTS = setDefaultLang(SUBJECTS, FALLBACK_LANG);
Constants.TRANSLATIONS.SMS = setDefaultLang(SMS, FALLBACK_LANG);

Constants.RIDE_NOTIFICATION = (function rideNotification () {
  const { BUTTONS, DRIVERCOPY, HEADINGS, RIDE_STATUS_MESSAGE } = RIDE_NOTIFICATION;
  const response = {
    HEADINGS,
    BUTTONS,
    DRIVERCOPY,
    RIDE_STATUS_MESSAGE,
    SOUND : 'ride_notification.wav',
  };
  response.BUTTONS = setDefaultLang(BUTTONS, FALLBACK_LANG);
  return response;
})();

export default Constants;
