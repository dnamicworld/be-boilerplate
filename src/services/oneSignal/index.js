// Third party modules
import axios from 'axios';

// In house modules
import Constants from 'services/constants';
const { OS: { PATH: path, APP_ID, API_KEY } } = Constants;

const options = {
  port    : 443,
  headers : {
    'content-type'  : 'application/json; charset=utf-8',
    'authorization' : `Basic ${API_KEY}`,
  },
};

const pushNotification = {
  mediaSettings : (imageUrl) => imageUrl ? ({
    large_icon      : imageUrl, // android large icon
    ios_attachments : imageUrl, // ios large icon
  }) : {},
  audioSettings : (soundUrl) => soundUrl ? ({
    ios_sound     : soundUrl,
    android_sound : soundUrl,
  }) : {},
  sendNotification : (playerId, notificationParams) => new Promise((resolve, reject) => {
    try {

      const { message, data, headings, subtitle, buttons, image, sound } = notificationParams;


      const messageData = {
        app_id             : APP_ID,
        include_player_ids : [playerId],
        contents           : message,
        headings, subtitle, buttons, data,
        ...pushNotification.mediaSettings(image),
        ...pushNotification.audioSettings(sound),
      };

      const jsonData = JSON.stringify(messageData);

      axios.post(`https://onesignal.com${path}`, jsonData, options)
        .then( ({ data : responseData }) => {

          console.log('push notification send', responseData);

          if (responseData.errors) {
            console.log('sendNotification error: ', responseData.errors);
            reject(responseData.errors);
          }

          resolve(responseData);

        }).catch(({ response, request, message : errorMsg }) => {

          console.log('sendNotification error: ', response);

          if (response) {
            reject(response.data);
          } else if (request) {
            reject(request);
          } else {
            reject(errorMsg);
          }

        });


    } catch (error) {
      reject(error);
    }
  }),
};

export default pushNotification;
