// Models
import LogModel from './model';

// In house modules
import { found } from 'services/defaultMessages';
import { notFound } from 'services/errors';

export default class LocationActions {

  constructor () {
    this.Model = LogModel;
    this.collectionName = this.Model.collection.collectionName;
  }

  getByDate = async (req, res) => {
    try {
      let { startDate, endDate } = req.query;
      startDate = new Date(startDate);
      endDate = new Date(endDate);
      startDate.setUTCHours(0, 0, 0, 0);
      endDate.setUTCHours(23, 59, 59, 999);
      const logs = await this.Model.find({ 'createdAt' : { '$gte' : startDate, '$lt' : endDate } });
      const msg = !logs ? 'Logs not found' : found(this.Model, logs);
      res.ok({ message : msg, data : logs });
    } catch (err) {
      res.badRequest(notFound('Error finding logs', err));
    }
  }
}
