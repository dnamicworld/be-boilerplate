// Third party.
import mongoose from 'mongoose';

// In-house modules.
import Constants from 'services/constants';

// Model schema.
const logSchema = new mongoose.Schema({
  author      : { type : mongoose.Schema.Types.ObjectId, ref : 'User', required : true },
  actionType  : { type : String, enum : Object.keys(Constants.LOG_ACTION_TYPES), required : true },
  model       : { type : String, required : true },
  description : { type : String },
  createdAt   : { type : Date, default : Date.now },
});

// Export the model.
export default mongoose.model('Log', logSchema);
