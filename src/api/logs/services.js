import LogModel from './model.js';

const LogServices = {
  saveLog (id, action, model, description) {
    new LogModel({
      author     : id,
      actionType : action,
      model,
      description,
    }).save();
  },
};

export default LogServices;
