import { Router } from 'express';
import Actions from './actions.js';

const router = new Router();
const actions = new Actions();

// GET methods
router.get('/getByDate', actions.getByDate);

export default router;
