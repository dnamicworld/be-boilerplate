// In house modules
import mailer from 'services/aws/ses';
import { awsSesError } from 'services/errors';

export default class TemplateActions {

  /**
   * @api {post} /templates/create Create new template
   * @apiName createTemplate
   * @apiGroup templates
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 201 Created
   {
      "errors": null,
      "response": {
        "message": "Template created successfully"
      }
    }
   *
   */

  create = (req, res) => mailer.createTemplate(req.body)
    .then(() => res.created({ message : 'Template created successfully' })).catch((err) => res.badRequest(awsSesError(err)));

  getAll = (req, res) => mailer.listTemplates(req.query)
    .then((data) => res.ok({ message : 'Template list retrieved successfully', data })).catch((err) => res.badRequest(awsSesError(err)));

  get = (req, res) => mailer.getTemplate(req.body)
    .then((data) => res.ok({ message : 'Template retrieved successfully', data })).catch((err) => res.badRequest(awsSesError(err)));

  /**
   * @api {post} /templates/update Update template
   * @apiName updateTemplate
   * @apiGroup templates
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "message": "Template updated successfully"
      }
    }
   *
   */

  update = (req, res) => mailer.updateTemplate(req.body)
    .then(() => res.ok({ message : 'Template updated successfully' })).catch((err) => res.badRequest(awsSesError(err)));


  delete = (req, res) => mailer.deleteTemplate(req.body)
    .then(() => res.ok({ message : 'Template deleted successfully' })).catch((err) => res.badRequest(awsSesError(err)));


  /**
   * @api {post} /templates/test Test template
   * @apiName testTemplate
   * @apiGroup templates
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "message": "Email test sent"
      }
    }
   *
   */

  sendTest = async (req, res) => {
    try {
      const { email, values, template } = req.body;
      await mailer.sendTemplateMail(email, template, values);
      res.ok({ message : 'Email test sent' });
    } catch (err) {
      res.badRequest(awsSesError(err));
    }
  };
}
