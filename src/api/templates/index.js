import { Router } from 'express';
import Actions from './actions';

const router = new Router();
const actions = new Actions();

// Post methods
router.post('/create', actions.create);
router.post('/test', actions.sendTest);
router.post('/get', actions.get);

router.get('/', actions.getAll);

// Patch methods
router.patch('/update', actions.update);

// DELETE methods
router.delete('/delete', actions.delete);

export default router;
