import { Router } from 'express';
import Actions from './actions';

const router = new Router();
const actions = new Actions();

// Get methods
router.get('/', actions.isRunning);

export default router;
