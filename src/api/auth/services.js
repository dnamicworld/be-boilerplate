// Third party modules
import JWT from 'jsonwebtoken';
import _ from 'lodash';
import axios from 'axios';
import strng from 'string';
import async from 'async';

// In house modules
import Constants from 'services/constants';
import { unAuthorized } from 'services/errors';

const { AWS_SECRET, ROUTES : { USER } } = Constants;

const AuthServices = {

  issueToken  : (payload, time) => JWT.sign(payload, AWS_SECRET, { expiresIn : time }),
  verifyToken : (token) => new Promise((resolve, reject) => {
    JWT.verify(token, AWS_SECRET, (err, decoded) => {
      if (err) reject(err);
      resolve({
        valid   : true,
        content : decoded,
      });
    });
  }),

  checkRoute : (routes, path, method) => new Promise((resolve) => {
    const matched = _.find(routes[method], (route) => strng(path).include(route));
    const exist = _.includes(routes[method], matched);
    resolve(exist);
  }),

  getParts : (authorization) => {
    const parts = authorization.split(' ');
    return {
      scheme : parts[0],
      token  : parts[1],
      size   : parts.length,
    };
  },
  validFormatToken : (parts) => parts.size === 2 && /^Bearer$/i.test(parts.scheme),

  authErrMsg (msg) {
    const errMsg = msg ? msg : 'Require Authorization';
    return unAuthorized(errMsg);
  },

  checkAuthorization : (authorization) => new Promise((resolve, reject) => {

    const parts = AuthServices.getParts(authorization);
    const validFormat = AuthServices.validFormatToken(parts);

    if (!validFormat) reject();

    AuthServices.verifyToken(parts.token)
      .then(({ content }) => resolve(content))
      .catch((err) => reject(err));

  }),
  async authMidleware (req, res, next) {
    const { path, headers: { authorization }, method, query } = req;
    const { restricted } = query;
    try {
      const isPublic = await AuthServices.checkRoute(USER, path, method);
      if (restricted && method === 'GET') {
        _.set(req, 'isRestricted', true);
        next();
      } else if (isPublic && !restricted) {
        if (authorization) {
          await AuthServices.checkAuthorization(authorization).then((tokenContent) => {
            _.set(req, 'user', tokenContent);
          }).catch(() => {

          });

        }
        next();
      } else if (authorization) {
        AuthServices.checkAuthorization(authorization).then((tokenContent) => {
          _.set(req, 'user', tokenContent);
          next();
        }).catch(() => {
          res.unauthorized(AuthServices.authErrMsg('Format is Authorization: Bearer [token]'));
        });

      } else {
        throw AuthServices.authErrMsg();
      }
    } catch (err) {
      res.unauthorized(AuthServices.authErrMsg(err.message));
    }
  },


  facebook : (accessToken, userId) => new Promise((resolve, reject) => { // Request to FB
    const API = `https://graph.facebook.com/${userId}?access_token=${accessToken}&fields=id,first_name,last_name,birthday,gender,verified,email`;
    axios.get(API).then(({ data }) => resolve(data)).catch((error) => reject(error));
  }),
  likesPaging : (next) => new Promise((resolve, reject) => {
    axios.get(next).then(({ data }) => JSON.parse(data)).catch((error) => reject(error));
  }),

  getFacebookLikes : (accessToken) => new Promise((resolve, reject) => {
    const limit = 100;
    const API = `https://graph.facebook.com/me/likes?limit=${limit}&access_token=${accessToken}`;
    const likesArray = [];
    axios.get(API).then(({ data }) => {
      let url = data.paging.next;
      likesArray.push(data.data);
      if (typeof url === 'undefined') resolve(likesArray);

      async.whilst(
        () => !_.isNull(url),
        (callback) => {
          AuthServices.likesPaging(url).then((likes) => {
            likesArray.push(likes.data);
            url = likes.data.length < limit ? null : likes.paging.next;

            callback(null, likesArray);
          });
        },
        (error, likesPlayer) => {
          if (error) reject(error);
          resolve(likesPlayer);
        }
      );
    }).catch(({ response }) => reject(response));
  }),
};

export default AuthServices;
