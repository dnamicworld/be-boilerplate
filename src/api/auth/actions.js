// Third party modules
import _ from 'lodash';

// Models
import UserModel from '../users/model';

import { sanitizeInput } from 'services/db';

// In house modules
import { issueToken } from './services';
import Constants from 'services/constants';
import {
  deactivated,
  emailRequired, emailNotExist,
  passRequired, emailPassRequired,
  phoneNotVerified, docsNotVerified,
  invalidToken,
} from 'services/errors';

const { TOKENS_TIME: { AUTH, REFRESH } } = Constants;

export default class AuthActions {
  /**
   * @api {post} /auth/user Login with user and password
   * @apiName auth
   * @apiGroup auth
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   * @apiParam {String} email User email address
   * @apiParam {String} password User password
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwiaWF0IjoxNDYyMjE4NzU2LCJleHAiOjE0NjIyMTk2NTZ9.rOsgQAz44okzkvJKuLjXCvKoPOOSuULEagU93b2Nik8",
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE0NjIyMTg3NTYsImV4cCI6MTQ2MzUxNDc1Nn0._31Z_Y_YKPWxx6Z4TlAcjaa6sSYcS2aweHOHOyy9FsU"
      },
      "message": "Authorized transaction"
    }
   *
   */

  async loginEmail (req, res, model, validateAppFields) {

    try {

      const { email, password } = req.body;

      if (!password && !email) throw emailPassRequired;

      if (!password) throw passRequired;

      if (!email) throw emailRequired;

      let emailSanitized = sanitizeInput(email);
      if (_.isEmpty(emailSanitized)) emailSanitized = '';
      let passSanitized = sanitizeInput(password);
      if (_.isEmpty(passSanitized)) passSanitized = '';

      const userEmail = _.trim(emailSanitized.toLowerCase());
      const userPass = _.trim(passSanitized);
      const userObj = await model.findOne({ email : { $eq : userEmail } });

      if (_.isNull(userObj)) throw emailNotExist;

      const { _id: userId, email: mail } = userObj;

      if (validateAppFields) validateAppFields(userObj);

      const passwordAccepted = await userObj.comparePassword(userPass);
      const tokenData = { userId, mail };

      if (passwordAccepted) {
        const tokens = {
          token   : issueToken(tokenData, AUTH),
          refresh : issueToken({ ...tokenData, type : 'refresh' }, REFRESH),
        };
        res.ok(Object.assign(tokens, { message : 'Authorized transaction' }));
      }
    } catch (err) {
      res.badRequest(err);
    }
  }

  validateAppFields = (user) => {

    const { phoneVerified, docsVerified, isActive } = user;
    if (!phoneVerified) throw phoneNotVerified;

    if (docsVerified !== undefined && !docsVerified) throw docsNotVerified;

    if (!isActive) throw deactivated;
  }

  user = (req, res) => this.loginEmail(req, res, UserModel, this.validateAppFields);

  /**
   * @api {get} /auth/refresh Refresh access token
   * @apiName refresh
   * @apiGroup auth
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
     "errors": null,
     "response": {
       "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwiaWF0IjoxNDYyMjE5MTM0LCJleHAiOjE0NjIyMjAwMzR9.Qpuiwb4g_PTGnmhbm7JcTC8Ur5q8f5aOK7tjnD5JP4Y",
       "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE0NjIyMTkxMzQsImV4cCI6MTQ2MzUxNTEzNH0.EvObgBH_b6Vkoltq9sPcUvwrsIsJSdnnk9EDGX7tMBU"
     },
     "message": "Authorized transaction"
   }
   *
   */

  refresh (req, res) {
    const { type, userId, mail } = req.user;

    if (type === 'refresh') {
      const refreshObj = { userId, mail };

      const tokens = {
        token   : issueToken(refreshObj, AUTH),
        refresh : issueToken(_.assign(refreshObj, { type : 'refresh' }), REFRESH),
      };
      res.ok(Object.assign(tokens, { message : 'Authorized transaction' }));
    } else { // this code never reached todo
      res.unauthorized(invalidToken);
    }
  }

}
