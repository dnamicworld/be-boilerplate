import { Router } from 'express';
import { authMidleware } from './auth/services.js';

const router = new Router();

/**
 * @apiDefine authorizationHeaders
 * @apiHeader (Headers) {String} Authorization JSON Web Token (Facebook Access Token if the endpoint is /auth)
 * @apiHeaderExample {json} Example
 *  {
 *    "Authorization": "Bearer JWT_TOKEN"
 *  }
 *
 * @apiErrorExample {json} Authorization Error
 HTTP/1.1 401 Unauthorized
 {
    "errors": {
      "code": 401,
      "content": {
        "error": "Require Authorization"
      }
    },
    "response": null
  }
 *
 */

/**
 * @apiDefine applicationError
 * @apiErrorExample {json} Application Error
 HTTP/1.1 400 Bad Request
 {
    "errors": {
      "code": codeNumber,
      "content": {
        "error": "friendly message"
      }
    },
    "response": null
  }
 *
 */

// API endpoints.
router.all('*', authMidleware);
router.use('/status', require('./server'));
router.use('/auth', require('./auth'));
router.use('/users', require('./users'));
router.use('/logs', require('./logs'));
router.use('/templates', require('./templates'));

export default router;
