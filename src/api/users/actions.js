import _ from 'lodash';

// Models
import UserModel from './model';

// In house modules
import { issueToken, facebook } from '../auth/services';
import { handleGender, handleBirthdayCase } from 'services';

import {
  fbIdRequired, fkTokenRequired,
  missingParams,
  notFound, validationError,
  phoneAlreadyExists, phoneNotVerified,
  fbDataUnshared,

  passRequired, emailBlacklisted,
  emailRequired, emailPassRequired,
  elementNotFound, phoneAndEmailExists,
  emailAlreadyExists,
} from 'services/errors';
import sharedEndpoints from 'api/shared/user-driver';
import { saveLog } from 'api/logs/services';
import { Query } from 'classes/Query';

import Constants from 'services/constants';
import notificationServices from 'services/notifications';
const {
  emails : { confirmEmail },
  sms : { sendVerificationSms }, pushNotifications,
} = notificationServices;
import { created, updated } from 'services/defaultMessages';
import { findById } from 'services/db';
import { isDisposableEmail, flagActive, decimalFlagsToResponseCode, difference } from 'services/services';

const {
  TOKENS_TIME: { ACTIVATE, AUTH, REFRESH },
  LOG_ACTION_TYPES : { CREATE, UPDATE },
  STATUS_OK, STATUS_IM_USED,
} = Constants;

export default class UserActions extends Query {

  /**
   * @apiDefine userModel
   * @apiParam {String} firstName first name
   * @apiParam {String} lastName last name
   * @apiParam {String} email email
   * @apiParam {Object} others others
   * @apiParam {String} avatarUrl profile picture
   */


  constructor () {
    super(UserModel);
    this.Model = UserModel;
    this.collectionName = this.Model.collection.collectionName;
  }

  /**
   * @api {post} /users Create a user
   * @apiName createUser
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   * @apiUse userModel
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 201 Created
   {
      "errors": null,
      "response": {
        "message": "User created"
      }
    }
   *
   */

  create = async (req, res) => {
    const userDriver = req.body;
    const environmentUrl = req.environmentUrl();
    const { password, email, phone : createdPhone } = userDriver;

    try {

      if (!password && !email) throw emailPassRequired;
      if (!password) throw passRequired;
      if (!email) throw emailRequired;
      if (isDisposableEmail(email)) throw emailBlacklisted;

      await this.Model.validateSchema(userDriver).catch((err) => { res.badRequest(validationError(...err)); });

      this.emailPhoneUsed(email, createdPhone, this.Model).then(async () => {
        const newUserDriver = new this.Model(userDriver);
        await newUserDriver.save().catch((err) => res.badRequest(validationError(err)));

        const { _id: driverId, email : mail, phone, language, phoneVerified, fromWebsite } = newUserDriver;
        const token = await issueToken({ driverId, mail }, ACTIVATE);
        const response = { message : created(this.collectionName), data : { _id : driverId }, token };
        if (phone && !fromWebsite) {
          const variables = {
            language,
            fullName : newUserDriver.fullName,
            linkUrl  : `${environmentUrl}/${this.collectionName}/verify/${token}`,
          };

          confirmEmail(email, variables);
          if (!phoneVerified) {
            await sendVerificationSms({ driverId }, phone, language).then((tokenResponse) => {
              console.log('users sms send success');
              response.token = tokenResponse;
            }, (error) => {
              console.log('sms send error', error);
            });
          }

        }
        saveLog(newUserDriver.id, CREATE, this.collectionName, created(this.collectionName));
        res.created(response);
      }, (error) => res.badRequest(error));

    } catch (err) {
      res.badRequest(err);
    }
  }

  /**
   * @api {post} /user/update Updates current user
   * @apiName update
   * @apiGroup user
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   * @apiParam {String} firstName first name
   * @apiParam {String} lastName last name
   * @apiParam {String} email email
   * @apiParam {String} gender gender
   * @apiParam {Date} birthday birthday
   * @apiParam {String} password password
   * @apiParam {String} avatarUrl profile picture
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "modified": true
      },
      "message": "User profile successfully updated"
    }
   *
   */


  globalUpdate = (req, res, id) => {

    const updateData = req.body;
    const environmentUrl = req.environmentUrl();

    const response = { message : updated(this.collectionName) };
    const modifiedFromCms = req.user.userId !== id;

    return new Promise( async (resolve, reject) => {
      try {
        await this.Model.validateSchema(updateData).catch((err) => { reject(validationError(...err)); } );
        let targetEntity = await findById(id, this.Model, this.Model.excludeFields());
        if (!targetEntity) reject(elementNotFound(this.collectionName));


        let docsChanged = false;
        const { phone: phoneBefore, email: emailBefore } = targetEntity;
        const targetCloned = _.cloneDeep(targetEntity).toObject();

        const { standardCoordinates, position, others, device, phoneVerified } = updateData;
        if (!standardCoordinates && position && !modifiedFromCms) {
          updateData.position.coordinates.reverse();
        }
        if (position && !position.type) updateData.position.type = 'Point';

        const dataDifference = difference(updateData, targetCloned);
        const dataDifferenceCopy = Object.assign({}, dataDifference);

        if (_.has(dataDifference, 'others')) {
          dataDifference.others = others;
        }

        if (_.has(dataDifference, 'device')) {
          dataDifference.device = device;
        }

        if (!modifiedFromCms && dataDifference.accountNumber) delete dataDifference.accountNumber;

        targetEntity = Object.assign(targetEntity, dataDifference);

        if (_.has(dataDifferenceCopy, 'email')) {
          if (!modifiedFromCms) targetEntity.emailVerified = false;
        }
        if (_.has(dataDifferenceCopy, 'phone') && !phoneVerified) {
          if (!modifiedFromCms) targetEntity.phoneVerified = false;
        }

        if (_.has(dataDifferenceCopy, 'others.vehicle')) {
          const vehicleSensitiveFields = ['media', 'id'];
          const vehicleSensitiveUpdated = dataDifferenceCopy.others.vehicle.some(vehicle => vehicleSensitiveFields.reduce((result, currentKey) => _.has(vehicle, currentKey), false));
          if (vehicleSensitiveUpdated) {
            targetEntity.docsLastActivity = Date.now();
            if (!modifiedFromCms) targetEntity.docsVerified = false;
            docsChanged = true;
          }
        }

        const updatedEntity = await targetEntity.save().catch((err) => {
          if (err.errors) {
            console.log('error in update', err);
            const phoneInUsed = err.errors.phone;
            const emailInUsed = err.errors.email;

            if (phoneInUsed && emailInUsed) reject(phoneAndEmailExists);

            if (emailInUsed) reject(emailAlreadyExists);

            if (phoneInUsed) reject(phoneAlreadyExists);

          } else {
            reject(validationError(err));
          }
        });
        const { phone: phoneAfter, language, email : emailAfter } = updatedEntity;
        const flagResponse = [0, 0];
        const phoneChanged = phoneBefore !== phoneAfter;
        const emailChanged = emailBefore !== emailAfter;

        if (emailChanged || phoneChanged) {
          if (emailChanged) {
            const token = await issueToken({ id, emailAfter }, ACTIVATE);
            const variables = {
              language,
              fullName : targetEntity.fullName,
              linkUrl  : `${environmentUrl}/${this.collectionName}/verify/${token}`,
            };
            confirmEmail(emailAfter, variables);
          }
          if (phoneChanged) {
            flagResponse[0] = 1; // previously sent verification code now is hold in another endpoint
          }
        }
        if (docsChanged) {
          flagResponse[1] = 1;
        }
        const criticalDataModified = flagActive(flagResponse);
        let returnCode = STATUS_OK;
        if (criticalDataModified) {
          returnCode = decimalFlagsToResponseCode(STATUS_IM_USED, flagResponse);
          response.code = returnCode;
        }
        resolve([returnCode, response, updatedEntity]);

      } catch (error) {
        reject(error);
      }
    });
  }
  update = (req, res) => {
    const { userId } = req.user;
    this.globalUpdate(req, res, userId)
      .then(([returnCode, response]) => res.customSuccess(returnCode, response))
      .catch((error) => {
        console.log('update failed', error, 'date', new Date());
        res.notFound(error);
      });
  };

  updateById = (req, res) => {
    const { id } = req.params;
    this.globalUpdate(req, res, id).then(([returnCode, response, updatedEntity]) => {
      saveLog(id, UPDATE, this.collectionName, updated(this.collectionName));
      response.data = updatedEntity;
      res.customSuccess(returnCode, response);
    }, (error) => res.notFound(error));
  };

  /**
   * @api {delete} /users Deletes an user
   * @apiName deleteUser
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   * @apiParam {String} userId
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "message": "User successfully deleted"
      }
    }
   *
   */

  delete = (req, res) => sharedEndpoints.delete(req, res, this.Model);

  /**
   * @api {get} /users Gets current user
   * @apiName getProfile
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
    "errors": null,
    "response": {
      "message": "User profile successfully retrieved",
      "_id": "571fe66c119cfdec62c9cc47",
      "firstName": "Andres",
      "lastName": "García",
      "email": "andres.garcia@mailinator.com",
      "others": {
       "birthday": "12/11/92",
      },
      "verified": false,
      "verifiedAt": "2016-04-26T22:06:36.645Z",
    }
   }
   *
   */
  getProfile = (req, res) => sharedEndpoints.getProfile(req, res, this.Model);

  /**
   * @api {get} /users/password/:email User forgot password
   * @apiName forgotPassword
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "message": "check your email"
      }
    }
   *
   */

  forgotPassword = (req, res) => sharedEndpoints.forgotPassword(req, res, this.Model);

  /**
   * @api {get} /users/verify/:token Verifies an account
   * @apiName verify
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "valid": true,
        "content": {
          "userId": "5729817b11d8fa0877e2d693",
          "mail": "andres.garcia@mailinator.com",
          "iat": 1462338230,
          "exp": 1462339130
        }
      },
      "message": "Account successfully verified"
    }
   *
   */
  verify = (req, res) => sharedEndpoints.verify(req, res, this.Model);

  sendVerification = (req, res) => sharedEndpoints.sendVerification(req, res, this.Model);

  verifyPhoneCode = (req, res) => sharedEndpoints.verifyPhoneCode(req, res, this.Model);

  /**
   * @api {post} /user/changepasswordforgot/:token Changes the password from landing page
   * @apiName changePasswordForgot
   * @apiGroup user
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "modified": true
      },
      "message": "Password successfully changed"
    }
   *
   */

  changePasswordForgot = (req, res) => sharedEndpoints.changePasswordForgot(req, res, this.Model);

  /**
   * @api {post} /users/fb Login with Facebook
   * @apiName facebook
   * @apiGroup user
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   * @apiParam {String} facebookToken User facebook access token
   * @apiParam {String} userId User facebook Id
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3YjRjODYzNDc0ZDQ3MzBhMTZiNmEiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwiaWF0IjoxNDYyMjE5OTgxLCJleHAiOjE0NjIyMjA4ODF9.YZFD98XLL2tRiO5UsxklqBSGWlf7KSGpkHpMDdDdunU",
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3YjRjODYzNDc0ZDQ3MzBhMTZiNmEiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE0NjIyMTk5ODEsImV4cCI6MTQ2MzUxNTk4MX0.yLroosOjMLFA8eFM_tdNN4dCdZaYf7JsD3Pc9qLxyf4"
      },
      "message": "Authorized transaction"
    }
   *
   */

  fbSuccess = (user) => {
    const { _id: userId, email : mail } = user;
    const message = 'Authorized transaction';
    const tokens = {
      token   : issueToken({ userId, mail }, AUTH),
      refresh : issueToken({ userId, mail, type : 'refresh' }, REFRESH),
    };
    return ({ message, ...tokens, data : { _id : userId } });
  }

  notificationTest = (req, res) => {
    pushNotifications.rideNotification('44918d65-2606-4af1-beda-94b70d093dad', { rideId : '5b7f1849933ae14bcf3a6359', message : { en : 'test push' }, data : { rideId : '5b7f1849933ae14bcf3a6359', timestamp : +new Date() }, image : 'https://api.mapbox.com/styles/v1/alltruck/cjkblxgmg0zhm2sml0o3x9lw4/static/pin-s-a+f44(-84.1197643,9.9981413/-84.1197643,9.9981413),15/800x450?access_token=pk.eyJ1IjoiYWxsdHJ1Y2siLCJhIjoiY2ppYzljeXBtMDFrZzNwbzVnaG9iZXFzZyJ9.EMglXNHXmEEqmsEu_1QgEA', language : 'ES' })
      .then(({ data }) => { console.log('notification success'); res.ok({ message : 'success', data }); })
      .catch( err => { console.log('error sending notification', err); res.badRequest(err); });
  }

  fbPhoneNotVerified = (userId, phone, language) => new Promise( async (resolve, reject) => {
    try {
      await sendVerificationSms({ driverId : userId }, phone, language)
        .then((tokenResponse) => resolve(tokenResponse))
        .catch((err) => reject(err));
    } catch (error) {
      reject(error);
    }
  })

  fbPhoneNotVerifiedAction = (res) => async (user, successMessage) => {
    await this.fbPhoneNotVerified(user._id, user.phone, user.language).then((phoneToken) => {
      res.ok({ ..._.omit(successMessage, ['message']), phoneToken }, phoneNotVerified);
    }, (err) => { console.log('err sending phone verified', err); });
  }

  fbLogin = async (req, res) => {
    try {
      const { fbUserId, fbToken, phone, country, phoneVerified, region, device, language } = req.body;

      if (!fbUserId && !fbToken) throw missingParams;

      if (!fbUserId) throw fbIdRequired;

      if (!fbToken) throw fkTokenRequired;

      const faceData = await facebook(fbToken, fbUserId);

      const {
        error, id: facebookId, first_name: firstName,
        birthday, last_name: lastName, email : fbEmail,
        verified: emailVerified, gender: fbGender,
      } = faceData;

      if (error) throw error; // fb error

      const fbPhoneNotVerifiedAction = this.fbPhoneNotVerifiedAction(res);

      if (!phone) { // already exist used just by old apps

        const user = await this.Model.findOne({ $or : [ { facebookId }, { email : fbEmail } ] });
        if (user) { // login FB
          const successMessage = this.fbSuccess(user);
          if (!user.phoneVerified) {
            await fbPhoneNotVerifiedAction(user, successMessage);
          } else { // Success login
            if (!user.facebookId) {
              user.facebookId = facebookId;
              await user.save();
            }
            res.ok(successMessage);
          }
        } else {
          res.notFound(notFound('User not found'));
        }

      } else { // register/signup FB

        const gender = await handleGender(fbGender);
        const userBirthday = await handleBirthdayCase(birthday);

        let user = await this.Model.findOne({ email : fbEmail });
        const avatarUrl = `http://graph.facebook.com/${facebookId}/picture?type=large`;

        if (user) {
          const successResponse = this.fbSuccess(user);

          const mergeData = async (data) => {
            user = Object.assign(user, data);
            await user.save();
            res.ok(successResponse);
          };

          if (!user.phoneVerified) {
            if (phoneVerified) {
              mergeData({ phone, phoneVerified });
            } else {
              await fbPhoneNotVerifiedAction(user, successResponse);
            }

          } else {
            mergeData({ facebookId, avatarUrl, others : { ...user.others, gender, userBirthday } });
          }

        } else { // create

          if (!fbEmail) throw fbDataUnshared;

          // new workflow


          const userInfo = {
            firstName, lastName, emailVerified,
            avatarUrl, facebookId, phone,
            device, country, region, language,
            phoneVerified : !!phoneVerified,
            email         : fbEmail, password      : facebookId,
            others        : { gender, userBirthday },
          };

          user = await this.Model.create(userInfo).catch((err) => {
            console.log('err', err);
            if (err.errors) {
              const phoneInUsed = err.errors.phone;
              if (phoneInUsed) res.badRequest(phoneAlreadyExists);
            } else {
              res.badRequest(validationError(err));
            }
          });

          if (user) {
            const successResponse = this.fbSuccess(user);
            if (user.phoneVerified) { // new flow with phoneVerified true
              res.ok(successResponse);
            } else { // old flow
              await fbPhoneNotVerifiedAction(user, successResponse);
            }
          }


        }

      }

    } catch (err) {
      console.log('err', err);
      res.badRequest(err);
    }
  }


  /**
   * @api {get} /users/resend/:input Gets current user
   * @apiName resendVerification
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
    "errors": null,
    "response": {
      "message: "check your email"
    }
   }
   *
   */
  resendVerification = (req, res) => sharedEndpoints.resendVerification(req, res, this.Model);

  checkDuplicate = (req, res) => sharedEndpoints.checkDuplicate(req, res, this.Model);

  sendPushNotification = (req, res) => {
    res.ok({});
  }

}
