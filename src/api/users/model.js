import mongoose from 'mongoose';
const Schema = mongoose.Schema;
import { validateScheme } from 'services/db';
import sharedModel, { sharedValidationSchema,
  preSaveTrigger,
  virtuals,
  findOneAndUpdateTrigger,
  comparePassword } from 'api/shared/user-driver/model';

export const SchemaName = 'User';
const sharedModelProps = sharedModel(SchemaName);

const paymentSchema = new Schema({
  cardHolderName : { type : String },
  cardNumber     : { type : String },
  id             : { type : Number },
  // cvv            : { type : String },
  expirationDate : { type : String },
  favorite       : { type : Boolean },
  cardVendor     : { type : String },
});

const userSchema = new Schema({
  ...sharedModelProps,
  facebookId     : String,
  settings       : Object, // TODO unused?
  favoriteCardId : String,
  payed          : { type : Boolean, default : true },
  payment        : [paymentSchema],
}, { timestamps : true, toObject : { getters : true, setters : true, virtuals : true } });


const schema = {
  type       : 'object',
  properties : {
    facebookId     : { type : 'string' },
    favoriteCardId : { type : 'string' },
    payed          : { type : 'boolean' },
    payment        : {
      type       : 'object',
      properties : {
        cardHolderName : { type : 'string' },
        cardNumber     : { type : 'string' },
        id             : { type : 'number' },
        expirationDate : { type : 'string' },
        favorite       : { type : 'boolean' },
        cardVendor     : { type : 'string' },
      },
    },
    ...sharedValidationSchema,
  },
  required : [],
};

userSchema.virtual('fullName').get(virtuals.getName);

userSchema.statics.validateSchema = (obj, requiredFields) => {
  const schemaReference = Object.assign({}, schema);
  if (requiredFields) {
    schemaReference.required = requiredFields;
  }
  return validateScheme(obj, schemaReference);
};

userSchema.index({ 'phone' : 1, email : 1 }, {
  background              : true,
  partialFilterExpression : { 'phone' : { $exists : true }, email : { $exists : true } } }, (err, result) => {
  console.log('error creating index', err);
  console.log('result', result);
});

userSchema.statics.excludeFields = () => '-updatedAt -createdAt -__v -password -creditCards';

userSchema.statics.restrictedFields = () => ({
  values         : 'firstName lastName',
  justWithParams : true,
});

userSchema.statics.getHistoric = () => 'firstName lastName avatarUrl';

userSchema.pre('save', function hashPass (next) {
  preSaveTrigger.call(this, next);
});

userSchema.pre('findOneAndUpdate', function updateTrigger (next) {
  findOneAndUpdateTrigger.call(this, next);
});

userSchema.methods.comparePassword = function compareValidator (userPassword) {
  return comparePassword.call(this, userPassword);
};
export default mongoose.model(SchemaName, userSchema);
