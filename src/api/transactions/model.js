import mongoose from 'mongoose';
const { Schema } = mongoose;

import { validateScheme, fields } from 'services/db';

const transactionsModel = new mongoose.Schema({
  rideId            : { type : Schema.Types.ObjectId },
  status            : String,
  response          : [{}],
  description       : String, // could be used later in cms
  amount            : fields.decimalField,
  authorizationCode : fields.positiveNumber,
  externalReference : fields.positiveNumber,
}, { timestamps : true, toJSON : { getters : true, setters : true }, toObject : { getters : true, setters : true } });

const schema = {
  type       : 'object',
  properties : {
    rideId            : { type : 'string' },
    status            : { type : 'string' },
    amount            : { type : 'number', minimum : 0 },
    authorizationCode : { type : 'number', minimum : 0 },
    externalReference : { type : 'number', minimum : 0 },
  },
  required : [ 'rideId', 'status', 'amount'],
};


transactionsModel.statics.validateSchema = (obj) => validateScheme(obj, schema);


export default mongoose.model('Transaction', transactionsModel);
