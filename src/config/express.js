import express from 'express';
import compression from 'compression';
import logger from 'morgan';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import cors from 'cors';
import boolParser from 'express-query-boolean';
import customResponses from './responses';
import multiparty from 'connect-multiparty';
import requestIp from 'request-ip';
import { env } from './environment';

export default (app) => {
  app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, Content-Type, Accept, Authorization');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.header('X-powered-by', 'DNAMIC Software');
    next();
  });
  if (env !== 'production') {
    app.use(helmet());
    app.use('/docs', express.static(`${__dirname}/../../docs`));
    app.use(logger('dev'));
  } else {
    app.use(helmet({ hsts : false }));
  }
  app.use(requestIp.mw());
  app.use(multiparty());
  app.use(cors());
  app.use(compression());
  app.use(customResponses());
  app.use(bodyParser.json({ limit : '100mb' }));
  app.use(bodyParser.urlencoded({ limit : '100mb', extended : true }));
  app.use(boolParser());
};
