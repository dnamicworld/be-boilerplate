const { packageJson : { port, version } } = require('files');

import services from 'services';

const all = {
  env  : process.env.NODE_ENV || 'development',
  port : process.env.PORT || port,
};

all.envUrl = `${services.apiUrl(all.env)}/${version}`;

export default all;
