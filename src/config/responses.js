import {
  STATUS_CREATED, STATUS_OK,
  STATUS_BAD_REQUEST, STATUS_NOT_FOUND,
  STATUS_UNAUTHORIZED, STATUS_INTERNAL_ERROR,
} from 'services/constants';
import { responseHandler } from 'services';
const { packageJson: { version } } = require('files');

export default function customResponses () {
  return (req, res, next) => {

    res.ok = (resp, err = null) => res.status(STATUS_OK).json(responseHandler(err, resp));

    res.customSuccess = (statusCode, resp) => res.status(statusCode).json(responseHandler(null, resp));

    res.created = (response) => res.status(STATUS_CREATED).json(responseHandler(null, response));

    res.badRequest = (err, response = null) => res.status(STATUS_BAD_REQUEST).json(responseHandler(err, response));

    res.unauthorized = (err) => res.status(STATUS_UNAUTHORIZED).json(responseHandler(err, null));

    res.notFound = (err) => res.status(STATUS_NOT_FOUND).json(responseHandler(err, null));

    res.serverError = (err) => res.status(STATUS_INTERNAL_ERROR).json(responseHandler(err, null));

    req.environmentUrl = () => `${req.headers.host}/${version}`;

    next();
  };
}
