import mongoose from 'mongoose';

const { DB_USER, DB_HOSTNAME, DB_SHARD, DB_PORT, DB_PASS, DB_NAME } = process.env;


const clusterUrl = () => `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}/${DB_NAME}`;
const unclusterUrl = () => `mongodb://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}:${DB_PORT}/${DB_NAME}`;
const mongoUri = DB_SHARD ? clusterUrl() : unclusterUrl();

const mongooseConnectOptions = {
  autoReconnect     : true,
  reconnectTries    : Number.MAX_VALUE,
  reconnectInterval : 1000,
  useNewUrlParser   : true,
};

mongoose.Promise = global.Promise;

// mongoose.set('debug', true);

mongoose.connection.once('open', function mongooseConnection () {
  console.log('MongoDB connected', mongoUri);

  mongoose.connection.on('connected', function connected () {
    console.log('MongoDB event connected');
  });

  mongoose.connection.on('disconnected', function disconnected () {
    console.log('MongoDB event disconnected');
  });

  mongoose.connection.on('reconnected', function reconnected () {
    console.log('MongoDB event reconnected');
  });

  mongoose.connection.on('error', function error (err) {
    console.log('MongoDB event error: ' + err);
  });

  return;
});

mongoose.connect(mongoUri, mongooseConnectOptions, (err) => {
  if (err) {
    console.log('MongoDB connection error: ', err);
    throw err;
  }
});
