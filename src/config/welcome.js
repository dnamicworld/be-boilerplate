const { packageJson: { version } } = require('files');
export default (req, res) => res.send(`<h1>Boilerplate API</h1><h2>Version ${version}</h2>`);
