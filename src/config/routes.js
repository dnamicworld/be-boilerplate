const { packageJson: { version } } = require('files');
import welcome from './welcome';


export default (app) => {
  app.use(`/${version}/`, require('../api'));
  app.route('*').get(welcome); // Default route.
};
