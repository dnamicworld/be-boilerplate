import express from 'express';
const app = express();
require('config/db');
require('config/express')(app);
require('config/routes')(app);

export default app;
