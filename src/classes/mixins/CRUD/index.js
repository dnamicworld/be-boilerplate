import { deleteById, updateById } from 'services/db';
import { saveLog } from 'api/logs/services';
import {
  notFound,
  errorUpdating, errorCreating,
} from 'services/errors';
import {
  created, updated,
  deleted, errorDeleting,
} from 'services/defaultMessages';
import Constants from 'services/constants';

const {
  LOG_ACTION_TYPES : { CREATE, UPDATE, DELETE }, // UPDATE, DELETE
} = Constants;


const CRUD = SuperClass => class extends SuperClass {

  constructor (model) {
    super(model);
    this.Model = model;
    this.collectionName = this.Model.collection.collectionName;
  }

  create = async (req, res) => {
    try {
      const { userId } = req.user;
      await this.Model.validateSchema(req.body).catch((err) => {
        const valError = { validationError : err };
        throw valError;
      });
      const newDoc = new this.Model(req.body);
      await newDoc.save().catch((err) => {
        throw err;
      });
      saveLog(userId, CREATE, this.collectionName, created(this.collectionName));
      res.created({ message : created(this.collectionName), data : newDoc });
    } catch (err) {
      res.badRequest(errorCreating(this.collectionName, err));
    }
  }

  update = (req, res) => {
    try {
      const { id } = req.params;
      const { userId } = req.user;
      updateById(this.Model, id, req.body).then((doc) => {
        saveLog(userId, UPDATE, this.collectionName, updated(this.collectionName));
        res.ok({ message : updated(this.collectionName), data : doc });
      }, (error) => {
        console.log('error', error);
        res.notFound(errorUpdating(this.collectionName));
      });
    } catch (err) {
      console.log('err', err);
      res.badRequest(err);
    }
  }

  delete = async (req, res) => {
    try {
      const { id } = req.params;
      const { userId } = req.user;
      await deleteById(id, this.Model).then(() => {
        saveLog(userId, DELETE, this.collectionName, deleted(this.collectionName));
        res.ok({ message : deleted(this.collectionName) });
      }).catch(() => {
        throw notFound(errorDeleting(this.collectionName));
      });

    } catch (err) {
      res.badRequest(err);
    }
  }
};

export default CRUD;
