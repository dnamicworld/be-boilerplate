import autocannon from 'autocannon';
import { envUrl } from 'config/environment';

const argv = require('minimist')(process.argv.slice(2));

const { u : endpoint, c : connections = 100 } = argv;


const instance = autocannon({
  url        : `${envUrl}/${endpoint}`,
  connections,
  pipelining : 1, // default
  duration   : 10, // default
}, (err, results) => { // eslint-disable-line
  console.log('results', results);
  if (err) {
    console.log('benchmark error');
  }
});


autocannon.track(instance);

// this is used to kill the instance on CTRL-C
process.once('SIGINT', () => {
  instance.stop();
});
