import request from 'supertest'; // eslint-disable-line
const { packageJson: { version } } = require('files');
import welcome from 'config/welcome';
// import app from 'app.js';

describe('Api routes', () => {
  test('root url res.send() with default message', () => {
    const send = jest.fn();
    const res = { send };
    welcome({}, res);
    expect(send.mock.calls).toHaveLength(1); // should be called once
    // The first arg of the first call to the function was below message
    expect(send.mock.calls[0][0]).toBe(`<h1>Boilerplate API</h1><h2>Version ${version}</h2>`);
  });

  // test('hitting root url', (done) => {
  //   request(app)
  //     .get('/')
  //     .expect(200, 'Hello World!')
  //     .end((err) => {
  //       if (err) throw done(err);
  //       done();
  //     });
  // });

});
