// Postman pretest scripts they can be used at collection level or folder level

/* eslint-disable */

module.exports = function loadUtils() {
    let helpers = {
      generateQuery: function generateQuery(input) {
        var queryString = pm.environment.get(input).split('=');
        pm.environment.set(input + 'Key', queryString[0]);
        pm.environment.set(input + 'Value', queryString[1]);
      },
      appendObject: function generateObject(obj, prefix = '', input) {
         // const  prefixes = prefix.split("."); TODO handle nested references
        const pmValue = pm.environment.get(input);
        // const regexNestedObj = /\[\\"({.+})\\"]/g; // TODO resolve nested variables
        // const matches = regexNestedObj.exec(pmValue);
        // const result = matches ? `[${matches}]`: pmValue;
        obj[prefix] = JSON.parse(pmValue);
      }
    }
    return helpers;
}.toString().split(/\r?\n/);


function flatten (arr) {
  return arr.reduce((flat, toFlatten) => flat.concat(Array.isArray(toFlatten) ? flatten(toFlatten) : toFlatten), []);
};

function serialize (obj, prefix) { // $.param alike
  var str = [], key;
  for(key in obj) {
    if (obj.hasOwnProperty(key)) {
      var k = prefix ? prefix + `[${key}]` : `[${key}]`, val = obj[key];
      str.push((val && typeof val === "object") ?
        serialize(val, k) :
        k + "=" + val);
    }
  }
  return str;
}

function generateObject(input, prefix = '') { //original generateObjct function failed cant modify request obj
  var object = JSON.parse(pm.environment.get(input));
  let pmRequest = pm.request;
  let body = pmRequest.body.formdata.toJSON();
  let objNewAttributes = this.flatten(this.serialize(object)).map((element) => {
    const [key, value] = element.split('=');
    let prefixKey = prefix + key;
    const props = {
      disabled: false,
      key : prefixKey,
      type : "text",
      value
    }
    return props;
    request.data[prefixKey] = props;
  });
  body = body.concat(objNewAttributes);
  pmRequest.body.formdata = body;
};

//https://www.freeformatter.com/json-escape.html

// console usage https://www.getpostman.com/docs/postman/sending_api_requests/debugging_and_logs
