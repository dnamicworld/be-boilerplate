"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _helpers = require("services/helpers");

const Messages = {
  found: (collection = '', data) => {
    const elementsUnits = (0, _helpers.elementSingularity)(data);
    return `${collection} ${elementsUnits} found`;
  },
  schemaValidation: 'validation error',
  passUpdated: collection => `${collection} password successfully changed`,
  created: collection => `New ${collection} element created`,
  deleted: collection => `${collection} element successfully deleted`,
  updated: collection => `${collection} element successfully updated`,
  errorDeleting: collection => `Error deleting ${collection} element(s)`,
  createErrorMsg: collection => `Failed to create  ${collection} element(s)`,
  updateErrorMsg: collection => `Failed to update  ${collection} element(s)`
};
var _default = Messages;
exports.default = _default;
module.exports = exports.default;