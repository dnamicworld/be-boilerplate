"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const helpers = {
  elementSingularity: data => {
    let elementsUnits = data ? 'element' : 'element(s)';

    if (Array.isArray(data) && data.length > 1) {
      elementsUnits += 's';
    }

    return elementsUnits;
  },
  binaryInsert: (equalCriteria, byKey, element, array, startVal, endVal) => {
    // mutating function
    const length = array.length;
    const start = startVal !== undefined ? startVal : 0;
    const end = endVal !== undefined ? endVal : length - 1;
    const m = start + Math.floor((end - start) / 2);
    const current = byKey ? element[byKey] : element;

    if (length === 0) {
      array.push(element);
      return;
    }

    const currentAtPos = arrayPos => byKey ? array[arrayPos][byKey] : array[arrayPos];

    const greaterThanByKey = arrayPos => current < currentAtPos(arrayPos);

    const lessThanByKey = arrayPos => current > currentAtPos(arrayPos);

    const equalWithKey = arrayPos => current === currentAtPos(arrayPos);

    const greaterThanComparison = lessThanByKey(end);
    const lessThanComparison = greaterThanByKey(start);
    const lessThanHalveComparison = greaterThanByKey(m);
    const greaterThanHalveComparison = lessThanByKey(m);

    const insertPosition = (el, pos) => array.splice(pos, 0, el);

    const insertStart = el => insertPosition(el, start);

    const inserRight = (el, pos) => insertPosition(el, pos + 1);

    if (greaterThanComparison) {
      inserRight(element, end);
      return;
    }

    if (lessThanComparison) {
      insertStart(element);
      return;
    }

    const insertBasedCondition = position => {
      if (equalCriteria(element, array[position])) {
        // true if current is less in distance
        insertStart(element);
      } else {
        inserRight(element, position);
      }
    };

    if (equalWithKey(start)) {
      insertBasedCondition(start);
      return;
    }

    if (equalWithKey(end)) {
      insertBasedCondition(end);
      return;
    }

    if (lessThanHalveComparison) {
      helpers.binaryInsert(equalCriteria, byKey, element, array, start, m - 1);
      return;
    }

    if (greaterThanHalveComparison) {
      helpers.binaryInsert(equalCriteria, byKey, element, array, m + 1, end);
      return;
    }
  },
  setDefaultLang: (obj, defaultLang) => {
    const objCopy = Object.assign({}, obj);
    Object.keys(objCopy).forEach(key => {
      objCopy[key].fallback = objCopy[key][defaultLang];
    });
    return objCopy;
  },
  getLang: (obj, lang) => obj[lang] ? obj[lang] : obj.fallback,
  sleepInSeconds: seconds => new Promise(resolve => setTimeout(resolve, seconds * 1000))
};
var _default = helpers;
exports.default = _default;
module.exports = exports.default;