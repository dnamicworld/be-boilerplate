"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _services = require("services");

var _defaultMessages = require("./defaultMessages");

var _constants = require("services/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const Errors = {
  addingCreditCard: additionalData => (0, _services.createCustomError)(_constants.ERROR_ADDING_CREDITCARD, 'Error adding your credit card', additionalData),
  removingCreditCard: additionalData => (0, _services.createCustomError)(_constants.ERROR_REMOVING_CREDITCARD, 'Error removing your credit card', additionalData),
  invalidCredentials: (0, _services.createCustomError)(_constants.ERROR_INVALID_CREDENTIALS, 'Invalid credentials'),
  deactivated: (0, _services.createCustomError)(_constants.ERROR_DEACTIVATED, 'Your account is deactivated'),
  fkTokenRequired: (0, _services.createCustomError)(_constants.ERROR_FBTOKEN_REQUIRED, 'FbToken required'),
  fbIdRequired: (0, _services.createCustomError)(_constants.ERROR_FBID_REQUIRED, 'FbUserId required'),
  fbDataUnshared: (0, _services.createCustomError)(_constants.ERROR_FBDATA_NEEDED_UNSHARED, 'Fb required data not shared'),
  emailBlacklisted: (0, _services.createCustomError)(_constants.ERROR_EMAIL_BLACKLIST, 'Blacklisted email not allowed'),
  emailRequired: (0, _services.createCustomError)(_constants.ERROR_EMAIL_REQUIRED, 'Email required'),
  emailNotVerified: (0, _services.createCustomError)(_constants.ERROR_EMAIL_NOT_VERIFIED, 'User email not verified'),
  emailNotExist: (0, _services.createCustomError)(_constants.ERROR_EMAIL_DOES_NOT_EXIST, 'User email does not exists'),
  emailWasVerified: (0, _services.createCustomError)(_constants.ERROR_EMAIL_VERIFIED, 'Email already verified'),
  emailAlreadyExists: (0, _services.createCustomError)(_constants.ERROR_EMAIL_USED, 'Email already exists'),
  phoneAlreadyExists: (0, _services.createCustomError)(_constants.ERROR_PHONE_USED, 'Phone already exists'),
  phoneAndEmailExists: (0, _services.createCustomError)(_constants.ERROR_PHONE_AND_EMAIL_USED, 'Email and phone already exists'),
  emailPhoneNotExist: (0, _services.createCustomError)(_constants.ERROR_EMAIL_PHONE_NOT_EXIST, 'Email or phone number does not exist'),
  passRequired: (0, _services.createCustomError)(_constants.ERROR_PASSWORD_REQUIRED, 'Password required'),
  phoneRequired: (0, _services.createCustomError)(_constants.ERROR_PHONE_REQUIRED, 'Phone required'),
  phoneNotVerified: (0, _services.createCustomError)(_constants.ERROR_PHONE_NOT_VERIFIED, 'User phone not verified'),
  phoneWasVerified: (0, _services.createCustomError)(_constants.ERROR_PHONE_VERIFIED, 'Phone already verified'),
  emailPassRequired: (0, _services.createCustomError)(_constants.ERROR_PASSWORD_EMAIL_REQUIRED, 'Email and Password required'),
  invalidToken: (0, _services.createCustomError)(_constants.ERROR_INVALID_TOKEN, 'Invalid token'),
  docsNotVerified: (0, _services.createCustomError)(_constants.ERROR_DOCS_NOT_VERIFIED, 'User docs not verified'),
  awsSesError: error => (0, _services.createCustomError)(error.statusCode, error.message, _lodash.default.omit(error, ['message', 'statusCode', 'code'])),
  elementNotFound: collection => (0, _services.createCustomError)(_constants.STATUS_NOT_FOUND, `${collection} element(s) not found`),
  validationError: additionalData => Errors.badRequest(_defaultMessages.schemaValidation, additionalData),
  errorCreating: (collection, additionalData) => (0, _services.createCustomError)(_constants.STATUS_INTERNAL_ERROR, (0, _defaultMessages.createErrorMsg)(collection), additionalData),
  errorUpdating: (collection, additionalData) => (0, _services.createCustomError)(_constants.STATUS_NOT_FOUND, (0, _defaultMessages.updateErrorMsg)(collection), additionalData),
  notFound: (message, additionalData) => (0, _services.createCustomError)(_constants.STATUS_NOT_FOUND, message, additionalData),
  badRequest: (message, additionalData) => (0, _services.createCustomError)(_constants.STATUS_BAD_REQUEST, message, additionalData),
  missingParams: (0, _services.createCustomError)(_constants.STATUS_BAD_REQUEST, 'Missing params'),
  internalError: (message, additionalData) => (0, _services.createCustomError)(_constants.STATUS_INTERNAL_ERROR, message, additionalData),
  unAuthorized: (message, additionalData) => (0, _services.createCustomError)(_constants.STATUS_UNAUTHORIZED, message, additionalData),
  withoutRestricted: (0, _services.createCustomError)(_constants.STATUS_BAD_REQUEST, 'Restricted values not defined')
};
var _default = Errors;
exports.default = _default;
module.exports = exports.default;