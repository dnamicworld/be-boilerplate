"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _axios = _interopRequireDefault(require("axios"));

var _constants = _interopRequireDefault(require("services/constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  OS: {
    PATH: path,
    APP_ID,
    API_KEY
  }
} = _constants.default;
const options = {
  port: 443,
  headers: {
    'content-type': 'application/json; charset=utf-8',
    'authorization': `Basic ${API_KEY}`
  }
};
const pushNotification = {
  mediaSettings: imageUrl => imageUrl ? {
    large_icon: imageUrl,
    // android large icon
    ios_attachments: imageUrl // ios large icon

  } : {},
  audioSettings: soundUrl => soundUrl ? {
    ios_sound: soundUrl,
    android_sound: soundUrl
  } : {},
  sendNotification: (playerId, notificationParams) => new Promise((resolve, reject) => {
    try {
      const {
        message,
        data,
        headings,
        subtitle,
        buttons,
        image,
        sound
      } = notificationParams;

      const messageData = _objectSpread({
        app_id: APP_ID,
        include_player_ids: [playerId],
        contents: message,
        headings,
        subtitle,
        buttons,
        data
      }, pushNotification.mediaSettings(image), pushNotification.audioSettings(sound));

      const jsonData = JSON.stringify(messageData);

      _axios.default.post(`https://onesignal.com${path}`, jsonData, options).then(({
        data: responseData
      }) => {
        console.log('push notification send', responseData);

        if (responseData.errors) {
          console.log('sendNotification error: ', responseData.errors);
          reject(responseData.errors);
        }

        resolve(responseData);
      }).catch(({
        response,
        request,
        message: errorMsg
      }) => {
        console.log('sendNotification error: ', response);

        if (response) {
          reject(response.data);
        } else if (request) {
          reject(request);
        } else {
          reject(errorMsg);
        }
      });
    } catch (error) {
      reject(error);
    }
  })
};
var _default = pushNotification;
exports.default = _default;
module.exports = exports.default;