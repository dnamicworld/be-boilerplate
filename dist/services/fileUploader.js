"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _s3fs = _interopRequireDefault(require("s3fs"));

var _path = _interopRequireDefault(require("path"));

var _shortid = _interopRequireDefault(require("shortid"));

var _fs = _interopRequireDefault(require("fs"));

var _async = _interopRequireDefault(require("async"));

var _constants = _interopRequireDefault(require("./constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Third party modules
// In house modules
const S3FS = new _s3fs.default(_constants.default.BUCKET, {
  accessKeyId: _constants.default.S3_ID,
  secretAccessKey: _constants.default.S3_SECRET
});
const FilesActions = {
  upload: file => new Promise((resolve, reject) => {
    _async.default.map(file.files, (files, callback) => {
      const imgAttr = file.body;

      const ext = _path.default.extname(files.originalFilename);

      const imgName = _shortid.default.generate() + ext;
      const meta = {
        'ContentType': files.headers['content-type']
      };

      _fs.default.readFile(files.path, (err, data) => {
        S3FS.writeFile(`${imgAttr.path}/${imgName}`, data, meta).then(() => callback(null, {
          [files.fieldName]: `${_constants.default.S3_URL}/${imgAttr.path}/${imgName}`
        }), error => callback(error));
      });
    }, (err, result) => {
      if (err) reject(err);
      resolve(result);
    });
  }),
  delete: filePaths => new Promise((resolve, reject) => {
    _async.default.each(filePaths, (file, callback) => {
      S3FS.unlink(file.replace('https://s3-us-west-2.amazonaws.com/', '')).then(() => callback(), error => callback(error));
    }, err => {
      if (err) reject(err);
      resolve(true);
    });
  })
};
var _default = FilesActions;
exports.default = _default;
module.exports = exports.default;