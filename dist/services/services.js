"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _bitly = _interopRequireDefault(require("services/bitly"));

var _disposableEmailDomains = _interopRequireDefault(require("disposable-email-domains"));

var _csvtojson = require("csvtojson");

var _defaultMessages = require("./defaultMessages");

var _errors = require("./errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Third party modules
const fs = require('fs');

const Services = {
  isDisposableEmail: email => !!_disposableEmailDomains.default.find(domain => email.includes(domain)),

  decimalFlagsToResponseCode(base, flags) {
    const binary = flags.join('');
    return base + parseInt(binary, 2);
  },

  flagActive: flags => flags.some(flag => flag === 1),
  diffCond: val => _lodash.default.isObject(val) && !Services.plainArray(val),
  difference: (object, base) => _lodash.default.transform(object, (result, value, key) => {
    if (!_lodash.default.isEqual(value, base[key])) {
      result[key] = Services.diffCond(value) && Services.diffCond(base[key]) ? Services.difference(value, base[key]) : value;
    }
  }),
  plainArray: obj => Array.isArray(obj) && obj.every(el => !Array.isArray(el) && typeof el !== 'object' && typeof el !== 'function'),

  csvReload(Model, csvPath) {
    const converter = new _csvtojson.Converter({});
    const collectionName = Model.collection.collectionName;
    return new Promise((resolve, reject) => {
      converter.on('end_parsed', jsonResults => {
        Model.insertMany(jsonResults).then(categories => resolve(categories)).catch(err => reject((0, _errors.badRequest)(`Error creating ${collectionName}`, err)));
      });
      Model.remove({}, err => {
        if (err) reject((0, _defaultMessages.errorDeleting)(collectionName));
        const rs = fs.createReadStream(csvPath);
        rs.on('error', readError => reject(readError));
        rs.pipe(converter);
      });
    });
  },

  shortUrl: longUrl => new Promise((resolve, reject) => _bitly.default.shorten({
    longUrl
  }, (err, results) => {
    if (err) reject(err);
    const response = JSON.parse(results);
    resolve(response.data.url);
  })),
  parseError: err => ({
    name: err.name,
    message: err.message
  })
};
var _default = Services;
exports.default = _default;
module.exports = exports.default;