"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const Translations = {
  PAYMENT_ERROR: {
    EN: 'We had an issue processing your payment',
    ES: 'Tuvimos un inconveniente procesando su pago'
  },
  SMS: {
    VERIFICATION: {
      EN: 'Your AllTruck verification code is',
      ES: 'Tu codigo de verificación es'
    }
  },
  EMAILS: {
    ERROR_EMAIL: lang => {
      const response = {
        SUBJECT: {
          EN: 'Error with payment',
          ES: 'Error con tu pago'
        },
        COPY: {
          EN: 'We had an issue processing payment',
          ES: 'Tuvimos un error procesando tu pago'
        }
      };
      return Object.keys(response).reduce((result, key) => {
        result[key] = response[key][lang];
        return result;
      }, {});
    },
    SUBJECTS: {
      CONFIRM_EMAIL: {
        EN: 'Confirmation Email',
        ES: 'Email de confirmación'
      },
      DOCS_VERIFICATION: {
        EN: 'Verified Docs',
        ES: 'Documentos verificados'
      },
      RESEND_VERIFICATION: {
        EN: 'Resend verification',
        ES: 'Reenvio de verificación'
      },
      FORGOT_PASSWORD: {
        EN: 'Forgot password',
        ES: 'Olvidaste tu contraseña'
      },
      WELCOME: {
        EN: 'Welcome',
        ES: 'Bienvenid(a)'
      }
    }
  },
  RIDE_NOTIFICATION: {
    HEADINGS: {
      en: 'Accept your ride',
      es: 'Acepte el ride '
    },
    SUBTITLE: {
      en: 'subtitle test message',
      es: 'subtitulo mensaje de error'
    },
    DRIVERCOPY: {
      en: 'Would you like to accept the ride',
      es: 'Quieres aceptar el flete?'
    },
    BUTTONS: {
      accept: {
        EN: 'Accept',
        ES: 'Aceptar'
      },
      cancel: {
        EN: 'Cancel',
        ES: 'Cancelar'
      }
    },
    RIDE_STATUS_MESSAGE: status => ({
      en: `Your trip is in status : ${status}`,
      es: `Tu viaje esta en estado : ${status}`
    })
  }
};
var _default = Translations;
exports.default = _default;
module.exports = exports.default;