"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/es6.array.sort");

var _lodash = _interopRequireDefault(require("lodash"));

var _mongoSanitize = _interopRequireDefault(require("mongo-sanitize"));

var _schema = _interopRequireDefault(require("./schema"));

var _crud = _interopRequireDefault(require("./crud"));

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const DbServices = _objectSpread({}, _schema.default, _crud.default, {
  sanitizeInput: field => (0, _mongoSanitize.default)(field),
  projectSubFields: (subField, excludeFields) => excludeFields.split(' ').map(field => {
    let fieldResult = `${subField}.`;
    const exclude = field.split('-');

    if (exclude.length > 1) {
      fieldResult = `-${fieldResult}${exclude[1]}`;
    } else {
      fieldResult += `${exclude[0]}`;
    }

    return fieldResult;
  }).join(' '),
  spreadLanguages: (aggregation, code) => {
    aggregation.addFields({
      languages: {
        $filter: {
          input: '$languages',
          as: 'language',
          cond: {
            $eq: ['$$language.code', code]
          }
        }
      }
    });
    aggregation.unwind('languages');
    aggregation.addFields({
      name: '$languages.name',
      description: '$languages.description'
    });
    return aggregation;
  },
  vehicleAggregationByLanguage: (matchQuery, code, VehicleModel) => {
    // TODO REMOVED UNUSED
    const languageFilter = {
      languages: {
        $elemMatch: {
          code
        }
      }
    };
    const locationQuery = Object.assign(matchQuery, languageFilter);
    let vehiclesAggregation = VehicleModel.aggregate([{
      $match: locationQuery
    }, {
      $lookup: {
        from: 'rubbish',
        localField: 'serviceConfiguration.rubbishCategory',
        foreignField: '_id',
        as: 'serviceConfiguration'
      }
    }, {
      $addFields: {
        serviceConfiguration: {
          $filter: {
            input: '$serviceConfiguration',
            as: 'service',
            cond: {
              $eq: ['$$service.enabled', true]
            }
          }
        }
      }
    }]); //           arrivalFee           : '$arrivalFee.$numberDecimal',

    vehiclesAggregation = DbServices.spreadLanguages(vehiclesAggregation, code);
    vehiclesAggregation.project(`${VehicleModel.excludeFields()} -languages`);
    return vehiclesAggregation;
  },
  rubbishVehiclePopulate: Model => ({
    name: 'serviceConfiguration.rubbishCategory',
    fields: Model.excludeRubbish(),
    match: {
      enabled: true
    }
  }),
  vehicleQueryByLanguage: (matchQuery, code, queryOpt, Model) => {
    const languageFilter = {
      languages: {
        $elemMatch: {
          code
        }
      }
    };
    const composedQuery = Object.assign(matchQuery, languageFilter);
    return DbServices.findInCollectionByQuery(Model, composedQuery, Model.excludeFields(), queryOpt, DbServices.rubbishVehiclePopulate(Model));
  },
  formatVehicles: (elements, code) => elements.map(element => {
    const elementDestructured = DbServices.destructureLanguage(element.toObject({
      getters: true
    }), code);
    DbServices.destructureRubbish(elementDestructured, code);
    return elementDestructured;
  }),
  destructureRubbish: (vehicle, code) => {
    vehicle.serviceConfiguration = vehicle.serviceConfiguration.map(category => {
      const {
        rubbishCategory
      } = category;

      if (rubbishCategory) {
        rubbishCategory.languages = rubbishCategory.languages.filter(value => {
          return value.code === code;
        });
        return DbServices.destructureLanguage(rubbishCategory, code);
      }

      return category;
    });
    return vehicle;
  },
  destructureLanguage: (obj, code) => {
    let result = Object.assign({}, obj);

    if (result.languages.length) {
      let languageIndex = -1;
      result.languages.forEach((language, index) => {
        // eslint-disable-line consistent-return
        if (language.code === code) languageIndex = index;
      });
      result = Object.assign(result, _lodash.default.omit(result.languages[languageIndex], ['code', '_id']));
      delete result.languages;
    }

    return result;
  },
  checkRestricted: (params, justWithParams, restricted) => {
    return !restricted || !justWithParams || justWithParams && Object.keys(params).length;
  },
  getFlattenValues: (doc, current, obj, key) => {
    const result = current;
    Object.keys(obj).forEach(internalKey => {
      const newKey = `${key}-${internalKey}`;
      const isArray = Array.isArray(doc[key][internalKey]);
      const tempValue = isArray ? _lodash.default.head(doc[key][internalKey]) : doc[key][internalKey];
      const value = isArray ? tempValue[Object.keys(tempValue)[0]] : tempValue;
      result[_lodash.default.camelCase(newKey)] = _lodash.default.camelCase(newKey) === 'othersVehicle' ? `${value[0].car.brand} ${value[0].car.model}` : value;
    });
    const sorted = Object.keys(result).sort().reduce((acc, keyO) => _objectSpread({}, acc, {
      [keyO]: result[keyO]
    }), {});
    return sorted;
  },
  getCount: async (queriesContainer, queryInfo) => new Promise((resolve, reject) => {
    try {
      const {
        date: dateFilter,
        country
      } = queryInfo;
      const date = new Date(dateFilter);
      const momentDate = (0, _moment.default)(date.toISOString()).add(-1, 'd').endOf('day').valueOf();
      const countOperations = [];

      _lodash.default.forEach(queriesContainer, ({
        model,
        queries
      }) => {
        const formattedOperations = queries.map(({
          withDate,
          filter,
          populate,
          fields
        }) => {
          const customFilter = _objectSpread({}, filter, {
            country
          });

          const currentFilter = withDate ? _objectSpread({}, customFilter, {
            'detail.start': {
              $gte: momentDate
            },
            country
          }) : customFilter;
          const queryBuilder = model.find(currentFilter, fields, null);
          return _lodash.default.size(populate) > 0 ? DbServices.populateOpt(queryBuilder, populate) : queryBuilder;
        });
        countOperations.push(...formattedOperations);
      });

      Promise.all(countOperations).then(results => resolve(results)).catch(err => reject(err));
    } catch (err) {
      reject(err);
    }
  })
});

var _default = DbServices;
exports.default = _default;
module.exports = exports.default;