"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ajv = _interopRequireDefault(require("ajv"));

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const ajv = new _ajv.default({
  unknownFormats: 'ignore'
});

require('ajv-keywords')(ajv, 'uniqueItemProperties');

const {
  Schema
} = _mongoose.default;
const schemaServices = {
  validateScheme: (obj, scheme) => new Promise((resolve, reject) => {
    const valid = ajv.validate(scheme, obj);
    if (!valid) reject(ajv.errors);
    resolve(valid);
  }),
  numberToString: num => num.toString(),
  stringToNumber: decimal => decimal && decimal._bsontype === 'Decimal128' ? JSON.parse(decimal) : decimal,
  castId: id => _mongoose.default.Types.ObjectId(id),
  idString: id => id ? id.toString() : id
};
schemaServices.fields = {
  idField: {
    type: Schema.Types.ObjectId,
    get: schemaServices.idString,
    set: schemaServices.castId
  },
  positiveNumber: {
    type: Number,
    min: 0
  },
  decimalField: {
    type: Schema.Types.Decimal128,
    min: 0,
    set: schemaServices.numberToString,
    get: schemaServices.stringToNumber
  }
};
var _default = schemaServices;
exports.default = _default;
module.exports = exports.default;