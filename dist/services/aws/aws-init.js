"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _global = _interopRequireDefault(require("aws-sdk/global"));

var _ses = _interopRequireDefault(require("aws-sdk/clients/ses"));

var _sns = _interopRequireDefault(require("aws-sdk/clients/sns"));

var _constants = _interopRequireDefault(require("services/constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  AWS_REGION,
  AWS_SECRET,
  AWS_KEY
} = _constants.default;
_global.default.config.apiVersions = {
  sns: '2010-03-31' // other service API versions

};

_global.default.config.update({
  region: AWS_REGION,
  accessKeyId: AWS_KEY,
  secretAccessKey: AWS_SECRET
});

const awsServices = {
  SES: new _ses.default({
    apiVersion: '2010-12-01'
  }),
  SNS: new _sns.default()
};
const smsAttributes = {
  attributes: {
    DefaultSMSType: 'Transactional'
  }
};
awsServices.SNS.setSMSAttributes(smsAttributes, err => {
  if (err) console.log(err, err.stack); // an error occurred
});
var _default = awsServices;
exports.default = _default;
module.exports = exports.default;