"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _awsInit = require("./aws-init");

var _constants = _interopRequireDefault(require("services/constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// In house modules
const destinationParam = destination => typeof destination === 'string' ? {
  ToAddresses: [destination]
} : destination;

const getTemplateMailParams = (Source, destination, Template, TemplateData) => {
  const Destination = destinationParam(destination);
  return {
    Source,
    Destination,
    Template,
    TemplateData
  };
};

const getMailParams = (Source, destination, subject, Data) => {
  const Destination = destinationParam(destination);
  return {
    Source,
    Destination,
    Message: {
      Body: {
        Text: {
          Data
        }
      },
      Subject: {
        Data: subject
      }
    }
  };
};

const templateParams = ({
  templateName,
  htmlPart,
  subjectPart
}) => ({
  Template: {
    TemplateName: templateName,
    HtmlPart: htmlPart,
    SubjectPart: subjectPart
  }
});

const awsMailer = {
  sendTemplateMail: (destination, template, variables, from = _constants.default.AWS.EMAIL_SENDER) => {
    const params = getTemplateMailParams(from, destination, template, JSON.stringify(variables));

    const sendPromise = _awsInit.SES.sendTemplatedEmail(params).promise();

    sendPromise.then(data => {
      console.log('sendTemplateMail success: ', data);
    }).catch(err => {
      console.log('sendTemplateMail error: ', err);
    });
  },
  sendEmail: (destination, subject, message, from = _constants.default.AWS.EMAIL_SENDER) => {
    const params = getMailParams(from, destination, subject, message);

    const sendPromise = _awsInit.SES.sendEmail(params).promise();

    sendPromise.then(data => {
      console.log('sendRawMail success: ', data);
    }).catch(err => {
      console.log('sendRawMail error: ', err);
    });
  },
  destinationWithCC: (to, cc = _constants.default.AWS.CC) => ({
    CcAddresses: [cc],
    ToAddresses: [to]
  }),
  createTemplate: data => _awsInit.SES.createTemplate(templateParams(data)).promise(),
  getTemplate: data => _awsInit.SES.getTemplate(data).promise(),
  listTemplates: data => _awsInit.SES.listTemplates(data).promise(),
  updateTemplate: data => _awsInit.SES.updateTemplate(templateParams(data)).promise(),
  deleteTemplate: data => _awsInit.SES.deleteTemplate(data).promise()
};
var _default = awsMailer;
exports.default = _default;
module.exports = exports.default;