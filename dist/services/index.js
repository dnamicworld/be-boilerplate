"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const GlobalServices = {
  responseHandler: (errors, response) => ({
    error: errors,
    response: response
  }),
  createCustomError: (code, message, additionalInfo = {}) => ({
    code,
    content: _objectSpread({}, additionalInfo, {
      error: message
    })
  }),
  apiUrl: env => {
    let url = 'http://localhost:1102';

    if (env === 'development') {
      url = 'https://dev.alltruckcr.com';
    } else if (env === 'dev-stable') {
      url = 'https://dev-stable.alltruckcr.com';
    } else if (env === 'staging') {
      url = 'https://staging.alltruckcr.com';
    } else if (env === 'production') {
      url = 'https://api.alltruckcr.com/';
    }

    return url;
  },
  generatePhoneCode: (codeLength = 4) => {
    return Math.floor(Math.random() * (Math.pow(10, codeLength - 1) * 9)) + Math.pow(10, codeLength - 1);
  },
  handleGender: gender => new Promise((resolve, reject) => {
    try {
      if (gender === 'male') {
        resolve('M');
      } else if (gender === 'female') {
        resolve('F');
      } else {
        resolve('N');
      }
    } catch (err) {
      reject(err);
    }
  }),
  handleBirthdayCase: facebookBirthday => new Promise((resolve, reject) => {
    try {
      if ((0, _moment.default)(facebookBirthday, 'MM/DD/YYYY', true).isValid()) {
        resolve(facebookBirthday);
      } else if ((0, _moment.default)(facebookBirthday, 'YYYY', true).isValid()) {
        resolve((0, _moment.default)(`01-01-${facebookBirthday}`, 'MM-DD-YYYY').format('L'));
      } else if ((0, _moment.default)(facebookBirthday, 'MM/DD', true).isValid()) {
        resolve((0, _moment.default)(`${facebookBirthday}/1960`, 'MM-DD-YYYY').format('L'));
      }

      resolve('01/01/1960');
    } catch (err) {
      reject(err);
    }
  })
};
var _default = GlobalServices;
exports.default = _default;
module.exports = exports.default;