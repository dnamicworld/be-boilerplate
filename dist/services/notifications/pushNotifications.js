"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _constants = _interopRequireDefault(require("services/constants"));

var _oneSignal = require("services/oneSignal");

var _helpers = require("services/helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

const {
  TRANSLATIONS: {
    PAYMENT_ERROR
  },
  RIDE_NOTIFICATION: {
    DRIVERCOPY,
    HEADINGS,
    BUTTONS,
    SOUND,
    RIDE_STATUS_MESSAGE
  }
} = _constants.default;
var _default = {
  rideNotification: (playerId, params) => {
    const {
      language,
      rideId
    } = params,
          anotherParams = _objectWithoutProperties(params, ["language", "rideId"]);

    const rideParams = _objectSpread({}, anotherParams, {
      message: DRIVERCOPY,
      headings: HEADINGS,
      sound: SOUND,
      // subtitle : SUBTITLE,
      buttons: [{
        id: rideId,
        text: (0, _helpers.getLang)(BUTTONS.accept, language)
      }, {
        id: 0,
        text: (0, _helpers.getLang)(BUTTONS.cancel, language)
      }]
    });

    return (0, _oneSignal.sendNotification)(playerId, rideParams);
  },
  paymentNotification: playerId => {
    const rideParams = {
      message: DRIVERCOPY,
      headings: {
        en: 'Accept your ride',
        es: 'Acepte el ride '
      }
    };
    return (0, _oneSignal.sendNotification)(playerId, rideParams);
  },
  rideUpdateNotification: (playerId, status) => (0, _oneSignal.sendNotification)(playerId, {
    message: RIDE_STATUS_MESSAGE(status)
  }),
  // eslint-disable-line new-cap
  paymentError: playerId => (0, _oneSignal.sendNotification)(playerId, {
    message: PAYMENT_ERROR
  })
};
exports.default = _default;
module.exports = exports.default;