"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _sns = require("services/aws/sns");

var _pushNotifications = _interopRequireDefault(require("./pushNotifications"));

var _emails = _interopRequireDefault(require("./emails"));

var _services = require("api/auth/services");

var _services2 = require("services");

var _constants = _interopRequireDefault(require("services/constants"));

var _helpers = require("services/helpers");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  TRANSLATIONS: {
    PAYMENT_ERROR,
    SMS: {
      VERIFICATION
    }
  },
  TOKENS_TIME: {
    ACTIVATE
  }
} = _constants.default;
const notificationServices = {
  sms: {
    confirmSms: (phone, urlShorten) => (0, _sns.sendSms)(phone, urlShorten, 'Confirmation SMS'),
    sendVerificationSms: (tokenInfo = {}, phone, language) => new Promise(async (resolve, reject) => {
      const phoneCode = await (0, _services2.generatePhoneCode)();
      const verificationMsg = `${(0, _helpers.getLang)(VERIFICATION, language)} ${phoneCode}`;
      (0, _sns.sendSms)(phone, verificationMsg, 'Verification SMS').then(() => resolve((0, _services.issueToken)(_objectSpread({}, tokenInfo, {
        phone,
        phoneCode
      }), ACTIVATE)), error => reject(error));
    }),
    errorSms: (phone, lang) => (0, _sns.sendSms)(phone, PAYMENT_ERROR[lang], 'Error SMS')
  },
  emails: _emails.default,
  pushNotifications: _pushNotifications.default
};
var _default = notificationServices;
exports.default = _default;
module.exports = exports.default;