"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _services = require("api/logs/services");

var _services2 = require("api/auth/services");

var _notifications = _interopRequireDefault(require("services/notifications"));

var _services3 = require("services/services");

var _errors = require("services/errors");

var _db = require("services/db");

var _defaultMessages = require("services/defaultMessages");

var _constants = _interopRequireDefault(require("services/constants"));

var _environment = require("config/environment");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  emails: {
    welcomeEmail,
    resendVerificationEmail,
    forgotPassEmail
  },
  sms: {
    confirmSms,
    sendVerificationSms
  }
} = _notifications.default;
const {
  CMS_URL,
  LOG_ACTION_TYPES: {
    UPDATE,
    DELETE
  },
  TOKENS_TIME: {
    ACTIVATE,
    FORGOT
  }
} = _constants.default;
const cmsUrl = CMS_URL(_environment.env); // eslint-disable-line new-cap

var _default = {
  delete: async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    const {
      id
    } = req.params;
    (0, _db.deleteById)(id, Model).then(user => {
      (0, _services.saveLog)(user._id, DELETE, collectionName, (0, _defaultMessages.deleted)(collectionName));
      res.ok({
        message: (0, _defaultMessages.deleted)(collectionName)
      });
    }, () => res.badRequest((0, _errors.notFound)((0, _defaultMessages.errorDeleting)(collectionName))));
  },
  getProfile: async (req, res, Model) => {
    const {
      userId
    } = req.user;
    if (!userId) res.badRequest(_errors.invalidToken);
    (0, _db.findById)(userId, Model, Model.excludeFields().replace('-creditCards', '')).then(userObj => res.ok({
      'data': userObj
    }), () => res.notFound((0, _errors.notFound)('Failed to retrieve profile')));
  },
  verify: async (req, res, Model) => {
    const {
      token
    } = req.params;
    const collectionName = Model.collection.collectionName;

    try {
      const tokenInfo = await (0, _services2.verifyToken)(token);
      const {
        content: {
          mail: email
        }
      } = tokenInfo;
      const verifiedDriver = await Model.findOneAndUpdate({
        email
      }, {
        emailVerified: true
      }, {
        new: true
      });

      if (verifiedDriver) {
        const variables = {
          user: verifiedDriver.fullName,
          language: verifiedDriver.language
        };
        welcomeEmail(verifiedDriver.email, variables);
        (0, _services.saveLog)(verifiedDriver.id, UPDATE, collectionName, `${collectionName} element verified`);
        const verifiedPagesSuccess = `${cmsUrl}/validateEmail/success`;
        res.redirect(verifiedPagesSuccess);
      } else {
        const verifiedPagesError = `${cmsUrl}/validateEmail/error`;
        res.redirect(verifiedPagesError);
      }
    } catch (err) {
      const msg = err.message || 'Failed to verify your account';
      res.unauthorized((0, _errors.unAuthorized)(msg));
    }
  },
  verifyPhoneCode: async (req, res, model) => {
    try {
      const {
        phoneCode
      } = req.body;
      const {
        token
      } = req.params;
      if (!phoneCode) throw (0, _errors.badRequest)('No phone verification code provided');
      await (0, _services2.verifyToken)(token).then(tokenInfo => {
        const {
          content: {
            phone,
            phoneCode: code
          }
        } = tokenInfo;
        if (parseInt(phoneCode, 10) !== code) throw (0, _errors.badRequest)('Phone code does not match');
        const update = {
          phoneVerified: true,
          verifiedAt: Date.now()
        };
        model.findOneAndUpdate({
          phone
        }, update, (err, user) => {
          if (err || !user) {
            res.notFound((0, _errors.notFound)('Failed to verify yor phone number'));
          } else {
            res.ok({
              message: 'Phone successfully verified'
            });
          }
        });
      }, error => res.badRequest((0, _errors.badRequest)(error.message, error)));
    } catch (err) {
      res.badRequest(err);
    }
  },
  forgotPassword: (req, res, Model) => {
    const {
      input
    } = req.params;
    const collectionName = Model.collection.collectionName;

    const forgotUrl = token => `${cmsUrl}/${collectionName}/reset/?token=${token}`;

    Model.findOne({
      $or: [{
        'phone': input
      }, {
        email: input
      }]
    }, async (err, driver) => {
      try {
        if (err || !driver) throw _errors.emailPhoneNotExist;
        const {
          _id: userId,
          phone,
          email,
          phoneVerified,
          language
        } = driver;

        if (phone === input) {
          if (!phoneVerified) throw _errors.phoneNotVerified;
          const token = await (0, _services2.issueToken)({
            userId,
            phone
          }, FORGOT);
          const urlShorten = await (0, _services3.shortUrl)(forgotUrl(token));
          confirmSms(phone, urlShorten).then(data => res.ok(_objectSpread({
            message: 'Sms send',
            token
          }, data)), () => res.badRequest((0, _errors.internalError)('internal error sending your verification')));
        }

        if (email === input) {
          const token = (0, _services2.issueToken)({
            userId,
            mail: email
          }, FORGOT);
          const urlShorten = await (0, _services3.shortUrl)(forgotUrl(token));
          const variables = {
            language,
            linkUrl: urlShorten // CURRENT_ENV

          };
          forgotPassEmail(email, variables);
          res.ok({
            message: 'check your email'
          });
        }
      } catch (error) {
        res.badRequest(error);
      }
    });
  },
  sendVerification: async (req, res, Model) => {
    const {
      phone
    } = req.params;
    const {
      language
    } = req.query;
    Model.findOne({
      phone
    }, async (err, driver) => {
      try {
        if (err) throw (0, _errors.internalError)('internal error with db');
        if (driver) throw _errors.phoneAlreadyExists;
        sendVerificationSms({}, phone, language).then(tokenResponse => res.ok({
          message: 'Sms send',
          token: tokenResponse
        }), () => res.badRequest((0, _errors.internalError)('internal error sending your verification')));
      } catch (error) {
        res.badRequest(error);
      }
    });
  },
  resendVerification: (req, res, Model) => {
    const collectionName = Model.collection.collectionName;
    const environmentUrl = req.environmentUrl();
    const {
      input
    } = req.params;
    Model.findOne({
      $or: [{
        phone: input
      }, {
        email: input
      }]
    }, async (err, driver) => {
      try {
        if (err || !driver) throw _errors.emailPhoneNotExist;

        if (driver.phone === input) {
          const {
            _id: driverId,
            phone,
            language,
            phoneVerified
          } = driver;
          if (phoneVerified) throw _errors.phoneWasVerified;
          await sendVerificationSms({
            driverId
          }, phone, language).then(tokenResponse => res.ok({
            message: 'Sms send',
            token: tokenResponse
          }), () => res.badRequest((0, _errors.internalError)('internal error sending your verification')));
        }

        if (driver.email === input) {
          const {
            _id: driverId,
            email: mail,
            language,
            emailVerified
          } = driver;
          if (emailVerified) throw _errors.emailWasVerified;
          const token = await (0, _services2.issueToken)({
            driverId,
            mail
          }, ACTIVATE);
          const variables = {
            language,
            linkUrl: `${environmentUrl}/${collectionName}/verify/${token}`
          };
          resendVerificationEmail(mail, variables);
          res.ok({
            message: 'check your email'
          });
        }
      } catch (error) {
        res.badRequest(error);
      }
    });
  },
  changePasswordForgot: async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;

    try {
      const {
        password
      } = req.body;
      if (!password) throw _errors.passRequired;
      const {
        token
      } = req.params;
      const tokenInfo = await (0, _services2.verifyToken)(token);
      const {
        content: {
          phone,
          mail
        }
      } = tokenInfo;
      const successMessage = (0, _defaultMessages.passUpdated)(collectionName);

      if (phone) {
        await Model.findOne({
          phone
        }, async (err, driver) => {
          if (err && !driver) res.badRequest((0, _errors.notFound)('Phone number not found'));
          driver.password = password;
          await driver.save();
          (0, _services.saveLog)(driver.id, UPDATE, collectionName, successMessage);
          res.ok({
            message: 'Password successfully changed'
          });
        });
      } else {
        const driver = await Model.findOne({
          email: mail
        });
        if (!driver) throw _errors.emailNotExist;
        driver.password = password;
        await driver.save();
        (0, _services.saveLog)(driver.id, UPDATE, collectionName, successMessage);
        res.ok({
          message: successMessage
        });
      }
    } catch (err) {
      const message = err.message || 'Error updating password';
      res.badRequest(Object.assign(err, message));
    }
  },
  passwordById: async (req, res, Model) => {
    const collectionName = Model.collection.collectionName;

    try {
      const {
        password
      } = req.body;
      if (!password) throw _errors.passRequired;
      const {
        id
      } = req.params;
      const {
        userId
      } = req.user;
      const updatedDriver = await Model.findOne({
        '_id': id
      });
      updatedDriver.password = password;
      await updatedDriver.save();
      (0, _services.saveLog)(userId, UPDATE, collectionName, `${collectionName} password updated`);
      res.ok({
        message: `${collectionName} password successfully updated`
      });
    } catch (err) {
      const message = err.message || 'Error updating password';
      res.badRequest(message);
    }
  },
  emailPhoneUsed: (email, phone, Model) => new Promise((resolve, reject) => {
    Model.findOne({
      $or: [{
        email
      }, {
        phone
      }]
    }).then(inUse => {
      if (inUse) {
        const {
          email: inUseEmail,
          phone: phoneInUse
        } = inUse;
        const emailUsed = inUseEmail === email;
        const phoneUsed = phoneInUse === phone;
        if (emailUsed && phoneUsed) reject(_errors.phoneAndEmailExists);
        if (emailUsed) reject(_errors.emailAlreadyExists);
        if (phoneUsed) reject(_errors.phoneAlreadyExists);
      }

      resolve();
    }, () => resolve());
  }),
  checkDuplicate: function checkDuplicate(req, res, Model) {
    const {
      email,
      phone
    } = req.query;
    const formattedPhone = `+${_lodash.default.trim(phone)}`;
    return this.emailPhoneUsed(email, formattedPhone, Model).then(() => res.ok({
      message: 'Email and phone available'
    }), error => res.badRequest(error));
  }
};
exports.default = _default;
module.exports = exports.default;