"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.preSaveTrigger = preSaveTrigger;
exports.findOneAndUpdateTrigger = findOneAndUpdateTrigger;
exports.comparePassword = comparePassword;
exports.verifyEmail = verifyEmail;
exports.verifyPhone = verifyPhone;
exports.default = exports.sharedValidationSchema = exports.virtuals = exports.hashPassword = void 0;

var _bcryptjs = _interopRequireDefault(require("bcryptjs"));

var _validators = require("services/validators");

var _db = require("services/db");

var _errors = require("services/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const hashPassword = password => new Promise((resolve, reject) => {
  _bcryptjs.default.genSalt(10, (error, salt) => {
    if (error) reject(error);

    _bcryptjs.default.hash(password, salt, (err, hash) => {
      if (err) reject(err);
      resolve(hash);
    });
  });
});

exports.hashPassword = hashPassword;
const virtuals = {
  getName: function getName() {
    // this refer to doc dont change to arrow funct
    return `${this.firstName} ${this.lastName}`;
  }
};
exports.virtuals = virtuals;

async function preSaveTrigger(next) {
  // Generate a salt and apply to a password with the hashed one
  if (!this.isModified('password')) return next(); // Only hash the password if it has been modified (or is new)

  await hashPassword(this.password).then(hashedPass => {
    this.password = hashedPass;
  }, error => next(error));
  return next();
}

async function findOneAndUpdateTrigger(next) {
  const update = this.getUpdate();
  const {
    password,
    phone,
    email
  } = update.$set;

  if (password) {
    await hashPassword(this.password).then(hashedPass => {
      this._update.password = hashedPass;
    });
  }

  if (phone) {
    this._update.phoneVerified = false;
  }

  if (email) {
    this._update.emailVerified = false;
  }

  next();
}

function comparePassword(userPassword) {
  return new Promise((resolve, reject) => {
    _bcryptjs.default.compare(userPassword, this.password, (err, match) => {
      if (match === false) {
        reject(_errors.invalidCredentials);
      }

      resolve(match);
    });
  });
}

function verifyEmail(SchemaName, value, done) {
  if (!this.isModified('email')) {
    return done(true);
  }

  return (0, _validators.uniqueness)(SchemaName, {
    email: value
  }, done);
}

function verifyPhone(SchemaName, value, done) {
  if (!this.isModified('phone')) {
    return done(true);
  } // value.phone because we are validating whole others obj


  return (0, _validators.uniqueness)(SchemaName, {
    'phone': value
  }, done);
}

const sharedValidationSchema = {
  firstName: {
    type: 'string'
  },
  lastName: {
    type: 'string'
  },
  email: {
    type: 'string',
    'format': 'email'
  },
  phone: {
    type: 'string'
  },
  password: {
    type: 'string'
  },
  area: {
    type: 'string'
  },
  language: {
    type: 'string'
  },
  others: {
    type: 'object'
  },
  device: {
    type: 'object',
    properties: {
      appVersion: {
        type: 'string'
      },
      brand: {
        type: 'string'
      },
      model: {
        type: 'string'
      },
      os: {
        type: 'string'
      },
      osVersion: {
        type: 'string'
      }
    }
  },
  playerId: {
    type: 'string'
  },
  avatarUrl: {
    type: 'string'
  },
  deviceId: {
    type: 'string'
  },
  verifiedAt: {
    type: 'string'
  },
  rating: {
    type: 'number',
    minimum: 0
  },
  phoneVerified: {
    type: 'boolean'
  },
  emailVerified: {
    type: 'boolean'
  },
  creditCards: {
    type: 'array',
    items: [{
      type: 'object'
    }]
  },
  isActive: {
    type: 'boolean'
  },
  inactiveReason: {
    type: 'object',
    properties: {
      id: {
        type: 'string'
      },
      description: {
        type: 'string'
      }
    }
  }
};
exports.sharedValidationSchema = sharedValidationSchema;

var _default = SchemaName => ({
  firstName: {
    type: String,
    trim: true
  },
  lastName: {
    type: String,
    trim: true
  },
  email: {
    type: String,
    lowercase: true,
    trim: true,
    required: true,
    validate: {
      isAsync: true,
      validator: function emailValidator(value, done) {
        return verifyEmail.call(this, SchemaName, value, done);
      },
      message: 'Email already in used'
    }
  },
  password: {
    type: String,
    required: true,
    trim: true
  },
  country: {
    type: String,
    trim: true,
    uppercase: true
  },
  region: {
    type: String,
    trim: true
  },
  language: {
    type: String,
    trim: true,
    uppercase: true
  },
  phone: {
    type: String,
    trim: true,
    required: true,
    validate: {
      isAsync: true,
      validator: function phoneValidator(value, done) {
        return verifyPhone.call(this, SchemaName, value, done);
      },
      message: 'Phone already in used'
    }
  },
  device: {
    appVersion: String,
    brand: String,
    model: String,
    os: String,
    osVersion: String
  },
  playerId: String,
  avatarUrl: String,
  deviceId: {
    type: String
  },
  verifiedAt: Date,
  rating: _objectSpread({}, _db.fields.positiveNumber, {
    default: 5
  }),
  phoneVerified: {
    type: Boolean,
    default: false
  },
  // phone verified
  emailVerified: {
    type: Boolean,
    default: false
  },
  isActive: {
    type: Boolean,
    default: true
  },
  // handle bans
  inactiveReason: {
    id: String,
    description: String
  }
});

exports.default = _default;