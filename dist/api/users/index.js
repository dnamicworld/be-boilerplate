"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _actions = _interopRequireDefault(require("./actions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = new _express.Router();
const actions = new _actions.default(); // Get methods

router.get('/verify/:token', actions.verify);
router.get('/me', actions.getProfile);
router.get('/resend/:input', actions.resendVerification);
router.get('/sendVerification/:phone', actions.sendVerification);
router.get('/password/:input', actions.forgotPassword);
router.get('/check', actions.checkDuplicate); // Post methods

router.post('/', actions.create); // router.post('/notificationTest', actions.notificationTest);

router.post('/fb', actions.fbLogin);
router.post('/validatePhone/:token', actions.verifyPhoneCode);
router.post('/resetpassword/:token', actions.changePasswordForgot); // Patch methods

router.patch('/me', actions.update); // CMS Endpoints
// Get

router.get('/:id', actions.getById);
router.get('/', actions.getByQuery); // Patch

router.patch('/:id', actions.updateById); // Delete methods

router.delete('/:id', actions.delete); // Boilerplate methods

router.get('/:id/pushnotification', actions.sendPushNotification);
var _default = router;
exports.default = _default;
module.exports = exports.default;