"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _model = _interopRequireDefault(require("./model"));

var _services = require("../auth/services");

var _services2 = require("services");

var _errors = require("services/errors");

var _userDriver = _interopRequireDefault(require("api/shared/user-driver"));

var _services3 = require("api/logs/services");

var _Query = require("classes/Query");

var _constants = _interopRequireDefault(require("services/constants"));

var _notifications = _interopRequireDefault(require("services/notifications"));

var _defaultMessages = require("services/defaultMessages");

var _db = require("services/db");

var _services4 = require("services/services");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  emails: {
    confirmEmail
  },
  sms: {
    sendVerificationSms
  },
  pushNotifications
} = _notifications.default;
const {
  TOKENS_TIME: {
    ACTIVATE,
    AUTH,
    REFRESH
  },
  LOG_ACTION_TYPES: {
    CREATE,
    UPDATE
  },
  STATUS_OK,
  STATUS_IM_USED
} = _constants.default;

class UserActions extends _Query.Query {
  /**
   * @apiDefine userModel
   * @apiParam {String} firstName first name
   * @apiParam {String} lastName last name
   * @apiParam {String} email email
   * @apiParam {Object} others others
   * @apiParam {String} avatarUrl profile picture
   */
  constructor() {
    super(_model.default);

    _defineProperty(this, "create", async (req, res) => {
      const userDriver = req.body;
      const environmentUrl = req.environmentUrl();
      const {
        password,
        email,
        phone: createdPhone
      } = userDriver;

      try {
        if (!password && !email) throw _errors.emailPassRequired;
        if (!password) throw _errors.passRequired;
        if (!email) throw _errors.emailRequired;
        if ((0, _services4.isDisposableEmail)(email)) throw _errors.emailBlacklisted;
        await this.Model.validateSchema(userDriver).catch(err => {
          res.badRequest((0, _errors.validationError)(...err));
        });
        this.emailPhoneUsed(email, createdPhone, this.Model).then(async () => {
          const newUserDriver = new this.Model(userDriver);
          await newUserDriver.save().catch(err => res.badRequest((0, _errors.validationError)(err)));
          const {
            _id: driverId,
            email: mail,
            phone,
            language,
            phoneVerified,
            fromWebsite
          } = newUserDriver;
          const token = await (0, _services.issueToken)({
            driverId,
            mail
          }, ACTIVATE);
          const response = {
            message: (0, _defaultMessages.created)(this.collectionName),
            data: {
              _id: driverId
            },
            token
          };

          if (phone && !fromWebsite) {
            const variables = {
              language,
              fullName: newUserDriver.fullName,
              linkUrl: `${environmentUrl}/${this.collectionName}/verify/${token}`
            };
            confirmEmail(email, variables);

            if (!phoneVerified) {
              await sendVerificationSms({
                driverId
              }, phone, language).then(tokenResponse => {
                console.log('users sms send success');
                response.token = tokenResponse;
              }, error => {
                console.log('sms send error', error);
              });
            }
          }

          (0, _services3.saveLog)(newUserDriver.id, CREATE, this.collectionName, (0, _defaultMessages.created)(this.collectionName));
          res.created(response);
        }, error => res.badRequest(error));
      } catch (err) {
        res.badRequest(err);
      }
    });

    _defineProperty(this, "globalUpdate", (req, res, id) => {
      const updateData = req.body;
      const environmentUrl = req.environmentUrl();
      const response = {
        message: (0, _defaultMessages.updated)(this.collectionName)
      };
      const modifiedFromCms = req.user.userId !== id;
      return new Promise(async (resolve, reject) => {
        try {
          await this.Model.validateSchema(updateData).catch(err => {
            reject((0, _errors.validationError)(...err));
          });
          let targetEntity = await (0, _db.findById)(id, this.Model, this.Model.excludeFields());
          if (!targetEntity) reject((0, _errors.elementNotFound)(this.collectionName));
          let docsChanged = false;
          const {
            phone: phoneBefore,
            email: emailBefore
          } = targetEntity;

          const targetCloned = _lodash.default.cloneDeep(targetEntity).toObject();

          const {
            standardCoordinates,
            position,
            others,
            device,
            phoneVerified
          } = updateData;

          if (!standardCoordinates && position && !modifiedFromCms) {
            updateData.position.coordinates.reverse();
          }

          if (position && !position.type) updateData.position.type = 'Point';
          const dataDifference = (0, _services4.difference)(updateData, targetCloned);
          const dataDifferenceCopy = Object.assign({}, dataDifference);

          if (_lodash.default.has(dataDifference, 'others')) {
            dataDifference.others = others;
          }

          if (_lodash.default.has(dataDifference, 'device')) {
            dataDifference.device = device;
          }

          if (!modifiedFromCms && dataDifference.accountNumber) delete dataDifference.accountNumber;
          targetEntity = Object.assign(targetEntity, dataDifference);

          if (_lodash.default.has(dataDifferenceCopy, 'email')) {
            if (!modifiedFromCms) targetEntity.emailVerified = false;
          }

          if (_lodash.default.has(dataDifferenceCopy, 'phone') && !phoneVerified) {
            if (!modifiedFromCms) targetEntity.phoneVerified = false;
          }

          if (_lodash.default.has(dataDifferenceCopy, 'others.vehicle')) {
            const vehicleSensitiveFields = ['media', 'id'];
            const vehicleSensitiveUpdated = dataDifferenceCopy.others.vehicle.some(vehicle => vehicleSensitiveFields.reduce((result, currentKey) => _lodash.default.has(vehicle, currentKey), false));

            if (vehicleSensitiveUpdated) {
              targetEntity.docsLastActivity = Date.now();
              if (!modifiedFromCms) targetEntity.docsVerified = false;
              docsChanged = true;
            }
          }

          const updatedEntity = await targetEntity.save().catch(err => {
            if (err.errors) {
              console.log('error in update', err);
              const phoneInUsed = err.errors.phone;
              const emailInUsed = err.errors.email;
              if (phoneInUsed && emailInUsed) reject(_errors.phoneAndEmailExists);
              if (emailInUsed) reject(_errors.emailAlreadyExists);
              if (phoneInUsed) reject(_errors.phoneAlreadyExists);
            } else {
              reject((0, _errors.validationError)(err));
            }
          });
          const {
            phone: phoneAfter,
            language,
            email: emailAfter
          } = updatedEntity;
          const flagResponse = [0, 0];
          const phoneChanged = phoneBefore !== phoneAfter;
          const emailChanged = emailBefore !== emailAfter;

          if (emailChanged || phoneChanged) {
            if (emailChanged) {
              const token = await (0, _services.issueToken)({
                id,
                emailAfter
              }, ACTIVATE);
              const variables = {
                language,
                fullName: targetEntity.fullName,
                linkUrl: `${environmentUrl}/${this.collectionName}/verify/${token}`
              };
              confirmEmail(emailAfter, variables);
            }

            if (phoneChanged) {
              flagResponse[0] = 1; // previously sent verification code now is hold in another endpoint
            }
          }

          if (docsChanged) {
            flagResponse[1] = 1;
          }

          const criticalDataModified = (0, _services4.flagActive)(flagResponse);
          let returnCode = STATUS_OK;

          if (criticalDataModified) {
            returnCode = (0, _services4.decimalFlagsToResponseCode)(STATUS_IM_USED, flagResponse);
            response.code = returnCode;
          }

          resolve([returnCode, response, updatedEntity]);
        } catch (error) {
          reject(error);
        }
      });
    });

    _defineProperty(this, "update", (req, res) => {
      const {
        userId
      } = req.user;
      this.globalUpdate(req, res, userId).then(([returnCode, response]) => res.customSuccess(returnCode, response)).catch(error => {
        console.log('update failed', error, 'date', new Date());
        res.notFound(error);
      });
    });

    _defineProperty(this, "updateById", (req, res) => {
      const {
        id
      } = req.params;
      this.globalUpdate(req, res, id).then(([returnCode, response, updatedEntity]) => {
        (0, _services3.saveLog)(id, UPDATE, this.collectionName, (0, _defaultMessages.updated)(this.collectionName));
        response.data = updatedEntity;
        res.customSuccess(returnCode, response);
      }, error => res.notFound(error));
    });

    _defineProperty(this, "delete", (req, res) => _userDriver.default.delete(req, res, this.Model));

    _defineProperty(this, "getProfile", (req, res) => _userDriver.default.getProfile(req, res, this.Model));

    _defineProperty(this, "forgotPassword", (req, res) => _userDriver.default.forgotPassword(req, res, this.Model));

    _defineProperty(this, "verify", (req, res) => _userDriver.default.verify(req, res, this.Model));

    _defineProperty(this, "sendVerification", (req, res) => _userDriver.default.sendVerification(req, res, this.Model));

    _defineProperty(this, "verifyPhoneCode", (req, res) => _userDriver.default.verifyPhoneCode(req, res, this.Model));

    _defineProperty(this, "changePasswordForgot", (req, res) => _userDriver.default.changePasswordForgot(req, res, this.Model));

    _defineProperty(this, "fbSuccess", user => {
      const {
        _id: userId,
        email: mail
      } = user;
      const message = 'Authorized transaction';
      const tokens = {
        token: (0, _services.issueToken)({
          userId,
          mail
        }, AUTH),
        refresh: (0, _services.issueToken)({
          userId,
          mail,
          type: 'refresh'
        }, REFRESH)
      };
      return _objectSpread({
        message
      }, tokens, {
        data: {
          _id: userId
        }
      });
    });

    _defineProperty(this, "notificationTest", (req, res) => {
      pushNotifications.rideNotification('44918d65-2606-4af1-beda-94b70d093dad', {
        rideId: '5b7f1849933ae14bcf3a6359',
        message: {
          en: 'test push'
        },
        data: {
          rideId: '5b7f1849933ae14bcf3a6359',
          timestamp: +new Date()
        },
        image: 'https://api.mapbox.com/styles/v1/alltruck/cjkblxgmg0zhm2sml0o3x9lw4/static/pin-s-a+f44(-84.1197643,9.9981413/-84.1197643,9.9981413),15/800x450?access_token=pk.eyJ1IjoiYWxsdHJ1Y2siLCJhIjoiY2ppYzljeXBtMDFrZzNwbzVnaG9iZXFzZyJ9.EMglXNHXmEEqmsEu_1QgEA',
        language: 'ES'
      }).then(({
        data
      }) => {
        console.log('notification success');
        res.ok({
          message: 'success',
          data
        });
      }).catch(err => {
        console.log('error sending notification', err);
        res.badRequest(err);
      });
    });

    _defineProperty(this, "fbPhoneNotVerified", (userId, phone, language) => new Promise(async (resolve, reject) => {
      try {
        await sendVerificationSms({
          driverId: userId
        }, phone, language).then(tokenResponse => resolve(tokenResponse)).catch(err => reject(err));
      } catch (error) {
        reject(error);
      }
    }));

    _defineProperty(this, "fbPhoneNotVerifiedAction", res => async (user, successMessage) => {
      await this.fbPhoneNotVerified(user._id, user.phone, user.language).then(phoneToken => {
        res.ok(_objectSpread({}, _lodash.default.omit(successMessage, ['message']), {
          phoneToken
        }), _errors.phoneNotVerified);
      }, err => {
        console.log('err sending phone verified', err);
      });
    });

    _defineProperty(this, "fbLogin", async (req, res) => {
      try {
        const {
          fbUserId,
          fbToken,
          phone,
          country,
          phoneVerified,
          region,
          device,
          language
        } = req.body;
        if (!fbUserId && !fbToken) throw _errors.missingParams;
        if (!fbUserId) throw _errors.fbIdRequired;
        if (!fbToken) throw _errors.fkTokenRequired;
        const faceData = await (0, _services.facebook)(fbToken, fbUserId);
        const {
          error,
          id: facebookId,
          first_name: firstName,
          birthday,
          last_name: lastName,
          email: fbEmail,
          verified: emailVerified,
          gender: fbGender
        } = faceData;
        if (error) throw error; // fb error

        const fbPhoneNotVerifiedAction = this.fbPhoneNotVerifiedAction(res);

        if (!phone) {
          // already exist used just by old apps
          const user = await this.Model.findOne({
            $or: [{
              facebookId
            }, {
              email: fbEmail
            }]
          });

          if (user) {
            // login FB
            const successMessage = this.fbSuccess(user);

            if (!user.phoneVerified) {
              await fbPhoneNotVerifiedAction(user, successMessage);
            } else {
              // Success login
              if (!user.facebookId) {
                user.facebookId = facebookId;
                await user.save();
              }

              res.ok(successMessage);
            }
          } else {
            res.notFound((0, _errors.notFound)('User not found'));
          }
        } else {
          // register/signup FB
          const gender = await (0, _services2.handleGender)(fbGender);
          const userBirthday = await (0, _services2.handleBirthdayCase)(birthday);
          let user = await this.Model.findOne({
            email: fbEmail
          });
          const avatarUrl = `http://graph.facebook.com/${facebookId}/picture?type=large`;

          if (user) {
            const successResponse = this.fbSuccess(user);

            const mergeData = async data => {
              user = Object.assign(user, data);
              await user.save();
              res.ok(successResponse);
            };

            if (!user.phoneVerified) {
              if (phoneVerified) {
                mergeData({
                  phone,
                  phoneVerified
                });
              } else {
                await fbPhoneNotVerifiedAction(user, successResponse);
              }
            } else {
              mergeData({
                facebookId,
                avatarUrl,
                others: _objectSpread({}, user.others, {
                  gender,
                  userBirthday
                })
              });
            }
          } else {
            // create
            if (!fbEmail) throw _errors.fbDataUnshared; // new workflow

            const userInfo = {
              firstName,
              lastName,
              emailVerified,
              avatarUrl,
              facebookId,
              phone,
              device,
              country,
              region,
              language,
              phoneVerified: !!phoneVerified,
              email: fbEmail,
              password: facebookId,
              others: {
                gender,
                userBirthday
              }
            };
            user = await this.Model.create(userInfo).catch(err => {
              console.log('err', err);

              if (err.errors) {
                const phoneInUsed = err.errors.phone;
                if (phoneInUsed) res.badRequest(_errors.phoneAlreadyExists);
              } else {
                res.badRequest((0, _errors.validationError)(err));
              }
            });

            if (user) {
              const successResponse = this.fbSuccess(user);

              if (user.phoneVerified) {
                // new flow with phoneVerified true
                res.ok(successResponse);
              } else {
                // old flow
                await fbPhoneNotVerifiedAction(user, successResponse);
              }
            }
          }
        }
      } catch (err) {
        console.log('err', err);
        res.badRequest(err);
      }
    });

    _defineProperty(this, "resendVerification", (req, res) => _userDriver.default.resendVerification(req, res, this.Model));

    _defineProperty(this, "checkDuplicate", (req, res) => _userDriver.default.checkDuplicate(req, res, this.Model));

    _defineProperty(this, "sendPushNotification", (req, res) => {
      res.ok({});
    });

    this.Model = _model.default;
    this.collectionName = this.Model.collection.collectionName;
  }
  /**
   * @api {post} /users Create a user
   * @apiName createUser
   * @apiGroup users
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   * @apiUse userModel
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 201 Created
   {
      "errors": null,
      "response": {
        "message": "User created"
      }
    }
   *
   */


}

exports.default = UserActions;
module.exports = exports.default;