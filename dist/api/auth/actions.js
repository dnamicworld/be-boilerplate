"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _lodash = _interopRequireDefault(require("lodash"));

var _model = _interopRequireDefault(require("../users/model"));

var _db = require("services/db");

var _services = require("./services");

var _constants = _interopRequireDefault(require("services/constants"));

var _errors = require("services/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  TOKENS_TIME: {
    AUTH,
    REFRESH
  }
} = _constants.default;

class AuthActions {
  constructor() {
    _defineProperty(this, "validateAppFields", user => {
      const {
        phoneVerified,
        docsVerified,
        isActive
      } = user;
      if (!phoneVerified) throw _errors.phoneNotVerified;
      if (docsVerified !== undefined && !docsVerified) throw _errors.docsNotVerified;
      if (!isActive) throw _errors.deactivated;
    });

    _defineProperty(this, "user", (req, res) => this.loginEmail(req, res, _model.default, this.validateAppFields));
  }

  /**
   * @api {post} /auth/user Login with user and password
   * @apiName auth
   * @apiGroup auth
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   * @apiParam {String} email User email address
   * @apiParam {String} password User password
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
      "errors": null,
      "response": {
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwiaWF0IjoxNDYyMjE4NzU2LCJleHAiOjE0NjIyMTk2NTZ9.rOsgQAz44okzkvJKuLjXCvKoPOOSuULEagU93b2Nik8",
        "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE0NjIyMTg3NTYsImV4cCI6MTQ2MzUxNDc1Nn0._31Z_Y_YKPWxx6Z4TlAcjaa6sSYcS2aweHOHOyy9FsU"
      },
      "message": "Authorized transaction"
    }
   *
   */
  async loginEmail(req, res, model, validateAppFields) {
    try {
      const {
        email,
        password
      } = req.body;
      if (!password && !email) throw _errors.emailPassRequired;
      if (!password) throw _errors.passRequired;
      if (!email) throw _errors.emailRequired;
      let emailSanitized = (0, _db.sanitizeInput)(email);
      if (_lodash.default.isEmpty(emailSanitized)) emailSanitized = '';
      let passSanitized = (0, _db.sanitizeInput)(password);
      if (_lodash.default.isEmpty(passSanitized)) passSanitized = '';

      const userEmail = _lodash.default.trim(emailSanitized.toLowerCase());

      const userPass = _lodash.default.trim(passSanitized);

      const userObj = await model.findOne({
        email: {
          $eq: userEmail
        }
      });
      if (_lodash.default.isNull(userObj)) throw _errors.emailNotExist;
      const {
        _id: userId,
        email: mail
      } = userObj;
      if (validateAppFields) validateAppFields(userObj);
      const passwordAccepted = await userObj.comparePassword(userPass);
      const tokenData = {
        userId,
        mail
      };

      if (passwordAccepted) {
        const tokens = {
          token: (0, _services.issueToken)(tokenData, AUTH),
          refresh: (0, _services.issueToken)(_objectSpread({}, tokenData, {
            type: 'refresh'
          }), REFRESH)
        };
        res.ok(Object.assign(tokens, {
          message: 'Authorized transaction'
        }));
      }
    } catch (err) {
      res.badRequest(err);
    }
  }

  /**
   * @api {get} /auth/refresh Refresh access token
   * @apiName refresh
   * @apiGroup auth
   * @apiVersion 0.1.0
   *
   * @apiUse authorizationHeaders
   * @apiUse applicationError
   *
   *
   * @apiSuccessExample {json} Success
   HTTP/1.1 200 OK
   {
     "errors": null,
     "response": {
       "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwiaWF0IjoxNDYyMjE5MTM0LCJleHAiOjE0NjIyMjAwMzR9.Qpuiwb4g_PTGnmhbm7JcTC8Ur5q8f5aOK7tjnD5JP4Y",
       "refresh": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiI1NzI3OTQ1YTNiYzhkNWFjMGRiM2FkZWQiLCJtYWlsIjoic2hpb24wOTNAZ21haWwuY29tIiwidHlwZSI6InJlZnJlc2giLCJpYXQiOjE0NjIyMTkxMzQsImV4cCI6MTQ2MzUxNTEzNH0.EvObgBH_b6Vkoltq9sPcUvwrsIsJSdnnk9EDGX7tMBU"
     },
     "message": "Authorized transaction"
   }
   *
   */
  refresh(req, res) {
    const {
      type,
      userId,
      mail
    } = req.user;

    if (type === 'refresh') {
      const refreshObj = {
        userId,
        mail
      };
      const tokens = {
        token: (0, _services.issueToken)(refreshObj, AUTH),
        refresh: (0, _services.issueToken)(_lodash.default.assign(refreshObj, {
          type: 'refresh'
        }), REFRESH)
      };
      res.ok(Object.assign(tokens, {
        message: 'Authorized transaction'
      }));
    } else {
      // this code never reached todo
      res.unauthorized(_errors.invalidToken);
    }
  }

}

exports.default = AuthActions;
module.exports = exports.default;