"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _ses = _interopRequireDefault(require("services/aws/ses"));

var _errors = require("services/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class TemplateActions {
  constructor() {
    _defineProperty(this, "create", (req, res) => _ses.default.createTemplate(req.body).then(() => res.created({
      message: 'Template created successfully'
    })).catch(err => res.badRequest((0, _errors.awsSesError)(err))));

    _defineProperty(this, "getAll", (req, res) => _ses.default.listTemplates(req.query).then(data => res.ok({
      message: 'Template list retrieved successfully',
      data
    })).catch(err => res.badRequest((0, _errors.awsSesError)(err))));

    _defineProperty(this, "get", (req, res) => _ses.default.getTemplate(req.body).then(data => res.ok({
      message: 'Template retrieved successfully',
      data
    })).catch(err => res.badRequest((0, _errors.awsSesError)(err))));

    _defineProperty(this, "update", (req, res) => _ses.default.updateTemplate(req.body).then(() => res.ok({
      message: 'Template updated successfully'
    })).catch(err => res.badRequest((0, _errors.awsSesError)(err))));

    _defineProperty(this, "delete", (req, res) => _ses.default.deleteTemplate(req.body).then(() => res.ok({
      message: 'Template deleted successfully'
    })).catch(err => res.badRequest((0, _errors.awsSesError)(err))));

    _defineProperty(this, "sendTest", async (req, res) => {
      try {
        const {
          email,
          values,
          template
        } = req.body;
        await _ses.default.sendTemplateMail(email, template, values);
        res.ok({
          message: 'Email test sent'
        });
      } catch (err) {
        res.badRequest((0, _errors.awsSesError)(err));
      }
    });
  }

}

exports.default = TemplateActions;
module.exports = exports.default;