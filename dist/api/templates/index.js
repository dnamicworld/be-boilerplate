"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = require("express");

var _actions = _interopRequireDefault(require("./actions"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = new _express.Router();
const actions = new _actions.default(); // Post methods

router.post('/create', actions.create);
router.post('/test', actions.sendTest);
router.post('/get', actions.get);
router.get('/', actions.getAll); // Patch methods

router.patch('/update', actions.update); // DELETE methods

router.delete('/delete', actions.delete);
var _default = router;
exports.default = _default;
module.exports = exports.default;