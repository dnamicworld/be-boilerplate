"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _model = _interopRequireDefault(require("./model"));

var _defaultMessages = require("services/defaultMessages");

var _errors = require("services/errors");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class LocationActions {
  constructor() {
    _defineProperty(this, "getByDate", async (req, res) => {
      try {
        let {
          startDate,
          endDate
        } = req.query;
        startDate = new Date(startDate);
        endDate = new Date(endDate);
        startDate.setUTCHours(0, 0, 0, 0);
        endDate.setUTCHours(23, 59, 59, 999);
        const logs = await this.Model.find({
          'createdAt': {
            '$gte': startDate,
            '$lt': endDate
          }
        });
        const msg = !logs ? 'Logs not found' : (0, _defaultMessages.found)(this.Model, logs);
        res.ok({
          message: msg,
          data: logs
        });
      } catch (err) {
        res.badRequest((0, _errors.notFound)('Error finding logs', err));
      }
    });

    this.Model = _model.default;
    this.collectionName = this.Model.collection.collectionName;
  }

}

exports.default = LocationActions;
module.exports = exports.default;