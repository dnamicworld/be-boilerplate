"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _constants = _interopRequireDefault(require("services/constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Third party.
// In-house modules.
// Model schema.
const logSchema = new _mongoose.default.Schema({
  author: {
    type: _mongoose.default.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  actionType: {
    type: String,
    enum: Object.keys(_constants.default.LOG_ACTION_TYPES),
    required: true
  },
  model: {
    type: String,
    required: true
  },
  description: {
    type: String
  },
  createdAt: {
    type: Date,
    default: Date.now
  }
}); // Export the model.

var _default = _mongoose.default.model('Log', logSchema);

exports.default = _default;
module.exports = exports.default;