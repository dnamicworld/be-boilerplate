"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _mongoose = _interopRequireDefault(require("mongoose"));

var _db = require("services/db");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  Schema
} = _mongoose.default;
const transactionsModel = new _mongoose.default.Schema({
  rideId: {
    type: Schema.Types.ObjectId
  },
  status: String,
  response: [{}],
  description: String,
  // could be used later in cms
  amount: _db.fields.decimalField,
  authorizationCode: _db.fields.positiveNumber,
  externalReference: _db.fields.positiveNumber
}, {
  timestamps: true,
  toJSON: {
    getters: true,
    setters: true
  },
  toObject: {
    getters: true,
    setters: true
  }
});
const schema = {
  type: 'object',
  properties: {
    rideId: {
      type: 'string'
    },
    status: {
      type: 'string'
    },
    amount: {
      type: 'number',
      minimum: 0
    },
    authorizationCode: {
      type: 'number',
      minimum: 0
    },
    externalReference: {
      type: 'number',
      minimum: 0
    }
  },
  required: ['rideId', 'status', 'amount']
};

transactionsModel.statics.validateSchema = obj => (0, _db.validateScheme)(obj, schema);

var _default = _mongoose.default.model('Transaction', transactionsModel);

exports.default = _default;
module.exports = exports.default;