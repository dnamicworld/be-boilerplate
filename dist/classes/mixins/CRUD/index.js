"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _db = require("services/db");

var _services = require("api/logs/services");

var _errors = require("services/errors");

var _defaultMessages = require("services/defaultMessages");

var _constants = _interopRequireDefault(require("services/constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const {
  LOG_ACTION_TYPES: {
    CREATE,
    UPDATE,
    DELETE
  } // UPDATE, DELETE

} = _constants.default;

const CRUD = SuperClass => class extends SuperClass {
  constructor(model) {
    super(model);

    _defineProperty(this, "create", async (req, res) => {
      try {
        const {
          userId
        } = req.user;
        await this.Model.validateSchema(req.body).catch(err => {
          const valError = {
            validationError: err
          };
          throw valError;
        });
        const newDoc = new this.Model(req.body);
        await newDoc.save().catch(err => {
          throw err;
        });
        (0, _services.saveLog)(userId, CREATE, this.collectionName, (0, _defaultMessages.created)(this.collectionName));
        res.created({
          message: (0, _defaultMessages.created)(this.collectionName),
          data: newDoc
        });
      } catch (err) {
        res.badRequest((0, _errors.errorCreating)(this.collectionName, err));
      }
    });

    _defineProperty(this, "update", (req, res) => {
      try {
        const {
          id
        } = req.params;
        const {
          userId
        } = req.user;
        (0, _db.updateById)(this.Model, id, req.body).then(doc => {
          (0, _services.saveLog)(userId, UPDATE, this.collectionName, (0, _defaultMessages.updated)(this.collectionName));
          res.ok({
            message: (0, _defaultMessages.updated)(this.collectionName),
            data: doc
          });
        }, error => {
          console.log('error', error);
          res.notFound((0, _errors.errorUpdating)(this.collectionName));
        });
      } catch (err) {
        console.log('err', err);
        res.badRequest(err);
      }
    });

    _defineProperty(this, "delete", async (req, res) => {
      try {
        const {
          id
        } = req.params;
        const {
          userId
        } = req.user;
        await (0, _db.deleteById)(id, this.Model).then(() => {
          (0, _services.saveLog)(userId, DELETE, this.collectionName, (0, _defaultMessages.deleted)(this.collectionName));
          res.ok({
            message: (0, _defaultMessages.deleted)(this.collectionName)
          });
        }).catch(() => {
          throw (0, _errors.notFound)((0, _defaultMessages.errorDeleting)(this.collectionName));
        });
      } catch (err) {
        res.badRequest(err);
      }
    });

    this.Model = model;
    this.collectionName = this.Model.collection.collectionName;
  }

};

var _default = CRUD;
exports.default = _default;
module.exports = exports.default;