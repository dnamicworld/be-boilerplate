"use strict";

require('dotenv').load();

require('app-module-path/register');

require('./app');