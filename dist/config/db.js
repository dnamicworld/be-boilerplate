"use strict";

var _mongoose = _interopRequireDefault(require("mongoose"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  DB_USER,
  DB_HOSTNAME,
  DB_SHARD,
  DB_PORT,
  DB_PASS,
  DB_NAME
} = process.env;

const clusterUrl = () => `mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}/${DB_NAME}`;

const unclusterUrl = () => `mongodb://${DB_USER}:${DB_PASS}@${DB_HOSTNAME}:${DB_PORT}/${DB_NAME}`;

const mongoUri = DB_SHARD ? clusterUrl() : unclusterUrl();
const mongooseConnectOptions = {
  autoReconnect: true,
  reconnectTries: Number.MAX_VALUE,
  reconnectInterval: 1000,
  useNewUrlParser: true
};
_mongoose.default.Promise = global.Promise; // mongoose.set('debug', true);

_mongoose.default.connection.once('open', function mongooseConnection() {
  console.log('MongoDB connected', mongoUri);

  _mongoose.default.connection.on('connected', function connected() {
    console.log('MongoDB event connected');
  });

  _mongoose.default.connection.on('disconnected', function disconnected() {
    console.log('MongoDB event disconnected');
  });

  _mongoose.default.connection.on('reconnected', function reconnected() {
    console.log('MongoDB event reconnected');
  });

  _mongoose.default.connection.on('error', function error(err) {
    console.log('MongoDB event error: ' + err);
  });

  return;
});

_mongoose.default.connect(mongoUri, mongooseConnectOptions, err => {
  if (err) {
    console.log('MongoDB connection error: ', err);
    throw err;
  }
});