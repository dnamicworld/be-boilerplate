"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _welcome = _interopRequireDefault(require("./welcome"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  packageJson: {
    version
  }
} = require('files');

var _default = app => {
  app.use(`/${version}/`, require('../api'));
  app.route('*').get(_welcome.default); // Default route.
};

exports.default = _default;
module.exports = exports.default;