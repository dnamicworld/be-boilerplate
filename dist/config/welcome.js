"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

const {
  packageJson: {
    version
  }
} = require('files');

var _default = (req, res) => res.send(`<h1>Boilerplate API</h1><h2>Version ${version}</h2>`);

exports.default = _default;
module.exports = exports.default;