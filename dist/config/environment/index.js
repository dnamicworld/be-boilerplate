"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _services = _interopRequireDefault(require("services"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const {
  packageJson: {
    port,
    version
  }
} = require('files');

const all = {
  env: process.env.NODE_ENV || 'development',
  port: process.env.PORT || port
};
all.envUrl = `${_services.default.apiUrl(all.env)}/${version}`;
var _default = all;
exports.default = _default;
module.exports = exports.default;