"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = customResponses;

var _constants = require("services/constants");

var _services = require("services");

const {
  packageJson: {
    version
  }
} = require('files');

function customResponses() {
  return (req, res, next) => {
    res.ok = (resp, err = null) => res.status(_constants.STATUS_OK).json((0, _services.responseHandler)(err, resp));

    res.customSuccess = (statusCode, resp) => res.status(statusCode).json((0, _services.responseHandler)(null, resp));

    res.created = response => res.status(_constants.STATUS_CREATED).json((0, _services.responseHandler)(null, response));

    res.badRequest = (err, response = null) => res.status(_constants.STATUS_BAD_REQUEST).json((0, _services.responseHandler)(err, response));

    res.unauthorized = err => res.status(_constants.STATUS_UNAUTHORIZED).json((0, _services.responseHandler)(err, null));

    res.notFound = err => res.status(_constants.STATUS_NOT_FOUND).json((0, _services.responseHandler)(err, null));

    res.serverError = err => res.status(_constants.STATUS_INTERNAL_ERROR).json((0, _services.responseHandler)(err, null));

    req.environmentUrl = () => `${req.headers.host}/${version}`;

    next();
  };
}

module.exports = exports.default;