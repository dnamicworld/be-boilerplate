"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _environment = require("config/environment");

var _listenHandler = _interopRequireDefault(require("config/listenHandler"));

var _server = _interopRequireDefault(require("./server"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_server.default.listen(_environment.port, err => (0, _listenHandler.default)(err, _environment.port));

var _default = _server.default;
exports.default = _default;
module.exports = exports.default;